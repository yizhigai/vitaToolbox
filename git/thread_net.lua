
UPDATE_PORT = channel.new("UPDATE_PORT")

local info = http.get("https://gitee.com/yizhigai/vitaToolbox/raw/master/version")
if not info then
	UPDATE_PORT:push({false, "", ""})
	return
else
	info /= "\n"
	local version = info[1]
	if not version or not tonumber(version) then
		UPDATE_PORT:push({false, "", ""})
		return
    else
		version = tonumber(version)
        UPDATE_PORT:push({true, version, tostring(info[2] or "")})
	end    
end
