--[[
	Updater Of App.
	Designed by DevDavisNunez to ONElua projects.. :D
	TODO:
	Maybe, extract in APP, and only installdir in this..
]]

buttons.homepopup(0)
color.loadpalette()
update = image.load("update.png")

function drawProgress(percent)
	if update then update:blit(0,0) end
	screen.print(480, 320, string.format("%s%%", percent or 0), 1, color.white, color.black, __ACENTER)
	draw.fillrect(0, 350, 960, 30, 0xFD444444)
	draw.fillrect(0, 350, (960/100)*percent, 30, color.green)
	screen.flip()
end

drawProgress(0)

args = os.arg()
if args:len() == 0 then
	os.message("Error args lost!")
	os.exit()
end

args /= "&"
if #args != 3 then
	os.message("Error args lost!")
	os.exit()
end

function onAppInstall(step, size, written, file, totalsize, totalwritten)
	if step == 2 then
		return 10
	elseif step == 3 then
		if update then update:blit(0,0) end
		local percent = math.floor((totalwritten/totalsize)*99)
		screen.print(480, 320, string.format("%s%%", percent or 0), 1, color.white, color.black, __ACENTER)
		draw.fillrect(0, 350, 960, 30, 0xFD444444)
		draw.fillrect(0, 350, (960/100)*percent, 30, color.green)
		screen.flip()
	end
end

drawProgress(50)

game.installdir(args[3])
files.delete(args[3])

drawProgress(100)

buttons.homepopup(1)

game.launch(args[2])
