
--设置CPU
os.cpu(333)

--加载脚本
dofile("scripts/version.lua")
dofile("scripts/utils.lua")
dofile("scripts/colors.lua")
dofile("theme/theme.lua")
dofile("scripts/commons.lua")
dofile("scripts/musicplayer/musicplayer_commons.lua")
dofile("scripts/vita_commons.lua")
dofile("scripts/dialogs.lua")
dofile("scripts/callbacks.lua")

--按键设置
buttons.interval(10, 4) --连发延迟和间隔
--buttons.analogtodpad(60) --摇杆死区

--获取系统版本
VITA_SWVERSION = tonumber(os.swversion())

--检测软件新版本
checkUpdater()

--删除临时升级用的app
deleteUpdateApp()

--获取确定键
getButtonAssign()

--加载字体
loadFont()

--加载语言
loadLang()

--加载背景图
loadBackground()

--启动主界面
dofile("scripts/main/main.lua")

