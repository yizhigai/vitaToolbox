
----------创建备份根目录----------
function accountmanager_create_accountpath()
	
	if checkFolderExist(__ACCOUNT_PATH) then
		return
	end
	if not files.exists("ux0:") then
		return
	end
	accountmanager_list = nil
	ACCOUNTMANAGER_IS_LOADING = true
	WaitDialog(WAIT_EXECUTING)
	deletePath(__ACCOUNT_PATH)
	createFolder(__ACCOUNT_PATH)
	ACCOUNTMANAGER_IS_LOADING = false
	dismissDialog()

end

----------获取备份列表----------
function accountmanager_get_list()
	
	accountmanager_list = files.listdirs(__ACCOUNT_PATH)
	if accountmanager_list then
		table.sort(accountmanager_list, function(a,b) return string.lower(a.name) < string.lower(b.name) end)
		local k = 1
		for i = 1, #accountmanager_list do
			local filePath = accountmanager_list[k].path
			if not checkFileExist(filePath.."/system.dreg") or not checkFileExist(filePath.."/system.ireg") then
				table.remove(accountmanager_list, k)
			else
				k += 1
			end
		end
	end
	accountmanager_need_refresh = true

end

----------备份当前帐号----------
function save_account()

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	if __CURRENT_LOGIN_ID == nil or __CURRENT_LOGIN_ID == "" then
		unlock_home_key()
		MessageDialog(TIPS, ACCOUNTMANAGER_NO_LOGIN, BACK)
		return
	end

	local savePath = __ACCOUNT_PATH..__CURRENT_LOGIN_ID
	createFolder(savePath)
	if not checkFolderExist(savePath) then
		unlock_home_key()
		MessageDialog(TIPS, ACCOUNTMANAGER_SAVE_FAILED, BACK)
		return
	end
	os.saveaccount(savePath)
    if not checkFileExist(savePath.."/system.dreg") or not checkFileExist(savePath.."/system.ireg") then
		unlock_home_key()
		MessageDialog(TIPS, ACCOUNTMANAGER_SAVE_FAILED, BACK)
		return
	end
	copyPath(__MYPROFILE_PATH, savePath)
	accountmanager_get_list()
	for i = 1, #accountmanager_list do
		if accountmanager_list[i].name == __CURRENT_LOGIN_ID then
			accountmanager_pos = i
			accountmanager_list[i].mark = true
			break
		end
	end

	unlock_home_key()
	MessageDialog(TIPS, ACCOUNTMANAGER_SAVE_COMPLETED, BACK)
 
end   

----------清除当前帐号----------
function remove_account()

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	os.removeaccount()
	deletePath(__MYPROFILE_PATH)
	deletePath("ur0:user/00/trophy/data/sce_trop/TRPUSER.DAT")
	deletePath("ur0:user/00/trophy/data/sce_trop/sce_pfs/files.db")
	relink_mc()
	__CURRENT_LOGIN_ID = os.login()

	unlock_home_key()
	local state = MessageDialog(TIPS, ACCOUNTMANAGER_CLEAR_COMPLETED, CANCEL, POSITIVE)
	if state == 1 then
		WaitDialog(WAIT_EXECUTING)
		os.delay(200)
		power.restart()
	end
 
end

----------还原备份帐号----------
function restore_account()

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local savePath = accountmanager_list[accountmanager_pos].path
	os.restoreaccount(savePath)
	
	local myprofileBackupPath = savePath.."/"..getFileName(__MYPROFILE_PATH)
	if checkFileExist(myprofileBackupPath) then
		copyPath(myprofileBackupPath, getParentPath(__MYPROFILE_PATH))
	else
		deletePath(__MYPROFILE_PATH)
	end
	deletePath("ur0:user/00/trophy/data/sce_trop/TRPUSER.DAT")
	deletePath("ur0:user/00/trophy/data/sce_trop/sce_pfs/files.db")
	relink_mc()
	__CURRENT_LOGIN_ID = os.login()

	unlock_home_key()
	local state = MessageDialog(TIPS, string.format(ACCOUNTMANAGER_BACKUP_COMPLETED, accountmanager_list[accountmanager_pos].name), CANCEL, POSITIVE)
	if state == 1 then
		WaitDialog(WAIT_EXECUTING)
		os.delay(200)
		power.restart()
	end

end   

----------删除备份帐号----------
function delete_accountSaveData()

	WaitDialog(WAIT_EXECUTING)
	
	local deleteList = {}
	local posIsMarked = accountmanager_list[accountmanager_pos].mark
	if not posIsMarked then
		table.insert(deleteList, accountmanager_list[accountmanager_pos])
		deleteList[#deleteList].pos = accountmanager_pos
	else
		for i = 1, #accountmanager_list do
			if accountmanager_list[i].mark then
				table.insert(deleteList, accountmanager_list[i])
				deleteList[#deleteList].pos = i
			end
		end
	end
	local delete_counts = #deleteList
	local state = MessageDialog(TIPS, string.format(ACCOUNTMANAGER_DELETE_READY, delete_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	local tmpPos = accountmanager_pos
	for i = 1, delete_counts do
		if ProgressDialog(NOW_DELETING, deleteList[i].name, percent, CANCEL) == 0 then
			break
		end
		local ret = deletePath(deleteList[i].path)
		if ret == 1 then
			if accountmanager_pos > deleteList[i].pos then
				tmpPos -= 1
			end
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/delete_counts)*100)
		if ProgressDialog(NOW_DELETING, deleteList[i].name, percent, CANCEL) == 0 then
			break
		end		
	end
	accountmanager_pos = tmpPos
	accountmanager_get_list()
	
	unlock_home_key()
	dismissDialog()

	MessageDialog(TIPS, string.format(ACCOUNTMANAGER_DELETE_COMPLETED, completed_counts, failed_counts), BACK)

end

----------打开菜单----------
function accountmanager_open_menu()

	local entries = {
		{text = ACCOUNTMANAGER_MENU_SAVE, enable = true},
		{text = ACCOUNTMANAGER_MENU_CLEAR, enable = true},
		{text = ACCOUNTMANAGER_MENU_BACKUP, enable = false},
		{text = ACCOUNTMANAGER_MENU_DELETE, enable = false},
		{text = ACCOUNTMANAGER_MENU_ABOUT, enable = true},
	}

	if accountmanager_list and #accountmanager_list > 0 then
		entries[3].enable = true
		entries[4].enable = true	
	end
	local sel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if sel == 1 then
		local state = MessageDialog(TIPS, ACCOUNTMANAGER_SAVE_READY, CANCEL, POSITIVE)
		if state == 1 then
			save_account()
		end
	elseif sel == 2 then
		local state = MessageDialog(TIPS, ACCOUNTMANAGER_CLEAR_READY, CANCEL, POSITIVE)
		if state == 1 then
			remove_account()
		end
	elseif sel == 3 then
		local state = MessageDialog(TIPS, ACCOUNTMANAGER_CLEAR_READY, CANCEL, POSITIVE)
		if state == 1 then
			restore_account()
		end
	elseif sel == 4 then
		delete_accountSaveData()
	elseif sel == 5 then
		MessageDialog(ACCOUNTMANAGER_MENU_ABOUT, string.format(ACCOUNTMANAGER_MENU_ABOUT_MESSAGE, __ACCOUNT_PATH), BACK)
	end

end

