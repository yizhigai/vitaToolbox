accountmanager_pos = 1
accountmanager_need_refresh = true

function accountmanager_draw()

	local math_func = {}

	--按键说明
	local button_texts = {
		ACCOUNTMANAGER_BUTTON_MARK,
		ACCOUNTMANAGER_BUTTON_MENU,
		ACCOUNTMANAGER_BUTTON_POSITIVE,
		ACCOUNTMANAGER_BUTTON_BACK,
	}

	local limit = 18
	local top = 1
	local bottom = limit
	local pos_old = accountmanager_pos

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local list_x = padding_left
	local list_y = STATUS_BAR_HEIGHT + padding_top
	local list_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local login_w = 8 --当前账号标记
	local login_h = login_w
	local login_margin_top = (FONT_HEIGHT_DEFAULT - login_h)/2
	local login_margin_right = 5
	local login_x = list_x - login_margin_right - login_w
	local ordinal_x = list_x --序数
	local name_x_default = 78 --名称
	local name_x = name_x_default
	local name_x2 = SCREEN_WIDTH - padding_left
	local name_w = name_x2 - name_x_default
	local list_w = name_x2 - ordinal_x
	local mark_padding_top = 3 --标记
	local mark_x = list_x
	local mark_w = list_w
	local mark_h = FONT_HEIGHT_DEFAULT + mark_padding_top*2
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = list_h
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		if accountmanager_list and #accountmanager_list > 0 then
			local listLen = #accountmanager_list
			if accountmanager_need_refresh then
				top, bottom, accountmanager_pos = refreshListStates(top, accountmanager_pos, limit, listLen)
				scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
				accountmanager_need_refresh = false
			end
			if pos_old ~= accountmanager_pos then
				pos_old = accountmanager_pos
				name_x = name_x_default
			end
			--列表
			local mList_y = list_y
			for i = top, bottom do
				--标记
				if accountmanager_list[i].mark then
					local mark_y = mList_y - mark_padding_top
					draw.fillrect(mark_x, mark_y, mark_w, mark_h, COLOR_MARK) 
				end

				local text_color = COLOR_TEXT_DEFAULT
				if i == accountmanager_pos then
					text_color = COLOR_TEXT_POS
				end
				--当前账号备份提示
				if __CURRENT_LOGIN_ID and  accountmanager_list[i].name == __CURRENT_LOGIN_ID then
					draw.fillrect(login_x, mList_y + login_margin_top, login_w, login_h, COLOR_GREEN)
				end
				--序数
				screen.print(ordinal_x, mList_y, string.format("%03d", i), FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				--文件名
				if  i == accountmanager_pos and screen.textwidth(accountmanager_list[i].name or "") > name_w then
					name_x = screen.print(name_x, mList_y, accountmanager_list[i].name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, name_w)
				else
					screen.print(name_x_default, mList_y, accountmanager_list[i].name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, name_w)
				end
				mList_y += FONT_LINE_HEIGHT_DEFAULT
			end
			--滚动条
			if listLen > limit then
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
			end
		elseif not ACCOUNTMANAGER_IS_LOADING then
			screen.print(list_x, list_y, ACCOUNTMANAGER_NO_SAVEDATA, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK)
		end

		--顶部状态栏
		top_status_bar_draw(ACCOUNT_MANAGER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.up or buttons.analogly < -60 then --↑键设
			if accountmanager_list and #accountmanager_list > 0 then
				if accountmanager_pos > 1 then
					accountmanager_pos -= 1
					accountmanager_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if accountmanager_list and #accountmanager_list > 0 then
				if accountmanager_pos < #accountmanager_list then
					accountmanager_pos += 1
					accountmanager_need_refresh = true
				end
			end
		end
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end

		if buttons.square then --□键
			if accountmanager_list and #accountmanager_list > 0 then
				if accountmanager_list[accountmanager_pos].mark then
					accountmanager_list[accountmanager_pos].mark = false
				else
					accountmanager_list[accountmanager_pos].mark = true
				end
			end
		end
	
		if buttons.triangle then --△键
			accountmanager_open_menu()
		end
		
		if buttons[positive] then --确定键
			accountmanager_open_menu()
		end
	end
		
	return math_func
	
end

