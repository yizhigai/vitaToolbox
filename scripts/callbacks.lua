
-------------------解压文件--------------------
function onExtractFiles(size, written, file, totalsize, totalwritten)
 
	EXTRACT_CANCEL = false
	local fileName = getFileName(file)
	local percent = math.floor((totalwritten/totalsize)*100)
	if ProgressDialog(NOW_EXTRACTING, fileName, percent, CANCEL, nil) == 0 then
		WaitDialog(WAIT_CANCELING)
		EXTRACT_CANCEL = true
		return 0
	end
 
end

-------------------下载文件--------------------
function onNetGetFile(size, written, speed, step)
	
	DOWNLOAD_CANCEL = false
	local percent = math.floor((written/size)*100)
	if ProgressDialog(NOW_DOWNLOADING, DOWNLOAD_FILE_NAME, percent, CANCEL, nil) == 0 then
		WaitDialog(WAIT_CANCELING)
		DOWNLOAD_CANCEL = true
		return 0
	end

end

-------------------安装软件--------------------
function onAppInstall(step, size, written, file, totalsize, totalwritten)

	INSTALL_STEP = step
	INSTALL_CANCEL = false
	if step == 2 then --危险性确认
		return 10
	elseif step == 3 then --解压安装包
		local percent = math.floor((totalwritten/totalsize)*99)
		if ProgressDialog(NOW_INSTALLING, INSTALL_GAME_TITLE, percent, CANCEL, nil) == 0 then
			WaitDialog(WAIT_CANCELING)
			INSTALL_CANCEL = true
			return 0
		end
	end

	return 1
   
end
