-----------------------colors------------------------

----------主要----------
COLOR_RED             = 0xFF0000FF --红
COLOR_GREEN           = 0xFF00FF00 --绿
COLOR_BLUE            = 0xFFFF0000 --蓝

----------第二要----------
COLOR_CYAN            = 0xFFFFFF00 --青
COLOR_MAGENTA         = 0xFFFF00FF --品红
COLOR_YELLOW          = 0xFF00FFFF --黄

----------第三要----------
COLOR_AZURE           = 0xFFFF7F00 --蔚蓝
COLOR_VIOLET          = 0xFFFF007F --紫罗兰
COLOR_ROSE            = 0xFF7F00FF --粉红
COLOR_ORANGE          = 0xFF007FFF --橙黄
COLOR_CHARTREUSE      = 0xFF00FF7F --查特酒绿
COLOR_SPRING_GREEN    = 0xFF7FFF00 --春绿

----------灰阶----------
COLOR_WHITE           = 0xFFFFFFFF --白
COLOR_LITEGRAY        = 0xFFBFBFBF --浅灰
COLOR_GRAY            = 0xFF7F7F7F --灰
COLOR_DARKGRAY        = 0xFF3F3F3F --深灰
COLOR_BLACK           = 0xFF000000 --黑

