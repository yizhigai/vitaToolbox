
---------------获取父路径-----------------
function getParentPath(path)
 
  return path:match("(.*/)[^/]+/?$") or path:match("(.+:)[^/]+/?$") 
 
end

---------------获取文件名---------------------
function getFileName(path)
 
  return path:match(".*/([^/]+)/?$") or path:match(".+:([^/]+)/?$") or path
 
end

---------------获取根路径---------------------
function getRootPath(path)
 
  return path:match("^(.+:)") or path:match("^(/).*")
 
end

---------------获取扩展名-------------------
function getExtension(str)
  
  return str:match(".+%.(%w+)$")

end

---------------去除扩展名-------------------
function stripExtension(str)
  
  return str:match("(.+)%.%w+$") or str

end

---------------去除首尾空格---------------------------
function trimString(str)
 
  return str:match("^%s*(.-)%s*$")

end  

---------------去除尾部斜杠-------------------
function stripVirgule(str)
 
  return str:match("(.*)/$") or str

end

----------------判断文件存在----------------------
function checkFileExist(path)
 
  local input = io.open(path, "r")
  if input == nil then
    return false
  end
  input:close()

  return true
 
end

----------------判断文件夹存在-----------------------
function checkFolderExist(path)

  if not files.list(path) then
    return false
  end

  return true
 
end

----------------获取剩余空间-----------------------
function getFreeSpace(path)
 
  local dev = getRootPath(path)
  local info = os.devinfo(dev)
  if not info or not info.free then
    return -1
  end

  return info.free
 
end

----------------判断剩余空间足够-----------------------
function checkFreeSpace(path, size)
 
  local freespace = getFreeSpace(path)
  if freespace < 0 or freespace < size then
		return false
  end

  return true
 
end

----------------获取文件大小格式化字符串-----------------------
function getSizeFormat(size)
 
  return files.sizeformat(size)
 
end

----------------获取文件大小2-----------------------
function getFileSize2(path, states)
	
	local fileSize = 0
	
	if states then
		local pcrcent = math.floor((states.current/states.total)*100)
		local fileName = getFileName(path)
		if ProgressDialog(NOW_COUNTING, fileName, pcrcent, CANCEL) == 0 then
			return -10
		end
	end
	if checkFileExist(path) then
		fileSize += files.size(path)
  elseif checkFolderExist(path) then --文件夹
    local fileList = files.list(path)
    if fileList and #fileList > 0 then
			for i = 1, #fileList do
				local mSize = getFileSize2(fileList[i].path, states)
				if mSize == -10 then
					return -10
				end
				fileSize += mSize
			end
		end
  end

	return fileSize

end

----------------获取文件大小-----------------------
function getFileSize(path, states)
	
	local fileSize = getFileSize2(path, states)
	if fileSize == -10 then
		return -10
	end
	
  if states then
		states.current += 1
		local pcrcent = math.floor((states.current/states.total)*100)
		if ProgressDialog(NOW_COUNTING, nil, pcrcent, CANCEL) == 0 then
			return -10
		end
	end
	
	return fileSize

end
----------------获取文件数量2-----------------------
function getFileCounts2(path, states)

  local fileCounts = 1
  
	if states then
		local pcrcent = math.floor((states.current/states.total)*100)
		local fileName = getFileName(path)
		if ProgressDialog(NOW_COUNTING, fileName, pcrcent, CANCEL) == 0 then
			return -10
		end
	end

  if checkFolderExist(path) then
    local fileList = files.list(path)
    if fileList and #fileList > 0 then
			for i = 1, #fileList do
				local mCounts = getFileCounts2(fileList[i].path, states)
				if mCounts == -10 then
					return -10
				end
				fileCounts += mCounts
			end
    end
  end

  return fileCounts

end

----------------获取文件数量-----------------------
function getFileCounts(path, states)
	
	local fileCounts = getFileCounts2(path, states)
	if fileCounts == -10 then
		return -10
	end
	
  if states then
		states.current += 1
		local pcrcent = math.floor((states.current/states.total)*100)
		if ProgressDialog(NOW_COUNTING, nil, pcrcent, CANCEL) == 0 then
			return -10
		end
	end
	
	return fileCounts

end

----------------创建文件夹-----------------------
function createFolder(path)

  files.mkdir(path)
  if not checkFolderExist(path) then
    return -1
  end

  return 1
 
end

----------------创建文件-----------------------
function createFile(path)

  local parentPath = getParentPath(path)
  if not checkFolderExist(parentPath) then
    if createFolder(parentPath) ~= 1 then
      return -1
    end
  end
  local output = io.open(path, "w+")
  if output == nil then
    return -1
  end
  output:close()

  return 1
 
end
 
----------------删除文件-----------------
function deleteFile(path, states)
	
  local mPath = stripVirgule(path)
  local mName = getFileName(mPath)

  local pcrcent = 0
  if states then
		pcrcent = math.floor((states.current/states.total)*100)
		if ProgressDialog(NOW_DELETING, mName, pcrcent, CANCEL) == 0 then
			return 0
		end
	end
	files.delete(mPath)
	if checkFileExist(mPath) or checkFolderExist(mPath) then
		return -1
  end
  if states then
		states.current += 1
		pcrcent = math.floor((states.current/states.total)*100)
		if ProgressDialog(NOW_DELETING, mName, pcrcent, CANCEL) == 0 then
			return 0
		end
	end
	
	return 1

end

----------------删除路径-----------------
function deletePath(path, states)
 
  if checkFolderExist(path) then --文件夹
    local fileList = files.list(path)
    if not fileList or #fileList < 1 then
      return deleteFile(path, states)
    end
    for i = 1, #fileList do
      local ret = deletePath(fileList[i].path, states)
      if ret == 0 then
        return 0
      end
    end
  end

	return deleteFile(path, states)
	
end

----------------复制文件-----------------------
function copyFile(srcPath, dstPath, states)
 
  if string.lower(srcPath) == string.lower(dstPath) then
    return -2
  end
  local dstParentPath = getParentPath(dstPath)
  if not dstParentPath then
    return -1
  end
  if not checkFolderExist(dstParentPath) then
    if createFolder(dstParentPath) ~= 1 then
      return -1
    end
  end
	
  local input = io.open(srcPath, "rb")
  if input == nil then
    return -1
  end
  local output = io.open(dstPath, "wb+")
  if output == nil then
    input:close()
    return -1
  end
  local fileName = getFileName(srcPath)
  local buffSize = 64*1024
  while true do
    power.tick(__POWER_TICK_SUSPEND)
		if states then
			local pcrcent = math.floor((states.current/states.total)*100)
			local ret = ProgressDialog(NOW_COPYING, fileName, pcrcent, CANCEL)
			if ret == 0 then
				input:close()
				output:close()
				return 0
			end
		end
    local bytes = input:read(buffSize)
    if not bytes then
      break
    end
    output:write(bytes)
    output:flush()
    if states then
			local bytesLen = #bytes
			states.current += bytesLen
    end
  end
  input:close()
  output:close()

  return 1
  
end

----------------复制路径(路径→文件夹下)-----------------------
function copyPath(srcPath, dstPath, states)
 
  local mSrcPath = stripVirgule(srcPath)
  local mSrcName = getFileName(mSrcPath)
  local mDstPath = stripVirgule(dstPath).."/"..mSrcName

  if checkFileExist(mSrcPath) then --文件
    return copyFile(mSrcPath, mDstPath, states)
  elseif checkFolderExist(mSrcPath) then --文件夹
    local mSrcPathLower = string.lower(mSrcPath.."/")
    local mDstPathLower = string.lower(mDstPath.."/")
    local mSrcPathLen = string.len(mSrcPathLower)
    local mDstPathLen = string.len(mDstPathLower)
    --复制到同路径或子路径则返回失败
    if mDstPathLen >= mSrcPathLen and string.sub(mDstPathLower, 1, mSrcPathLen) == mSrcPathLower then
      return -2
    end
    if not checkFolderExist(mDstPath) then
      if createFolder(mDstPath) ~= 1 then
        return -1
      end
    end
    local fileList = files.list(mSrcPath)
    if not fileList or not #fileList or #fileList == 0 then
      return 1
    end
    local completed_counts = 0
    local listLen = #fileList
    for i = 1, listLen do
      local ret = copyPath(fileList[i].path, mDstPath, states)
      if ret == 0 then
        return 0
      elseif ret == 1 then
				completed_counts += 1
      end
    end
    if completed_counts < listLen then
			return -1
    end
  else
		return -1
  end

  return 1
 
end

__moveFile = files.move
----------------移动文件-----------------
function moveFile(srcPath, dstPath, states)
	
	local srcName = getFileName(srcPath)
  if string.lower(srcPath) == string.lower(dstPath.."/"..srcName) then
    return -2
  end

  local pcrcent = 0
  if states then
		pcrcent = math.floor((states.current/states.total)*100)
		local ret = ProgressDialog(NOW_MOVING, srcName, pcrcent, CANCEL)
		if ret == 0 then
			return 0
		end
	end
	if not checkFileExist(srcPath) and not checkFolderExist(srcPath) then
		return -1
  end
	__moveFile(srcPath, dstPath)
	if checkFileExist(srcPath) or checkFolderExist(srcPath) then
		return -1
  end
  if states then
		states.current += 1
		pcrcent = math.floor((states.current/states.total)*100)
		local ret = ProgressDialog(NOW_MOVING, srcName, pcrcent, CANCEL)
		if ret == 0 then
			return 0
		end
	end

  return 1

end

----------------移动路径------------------
function movePath(srcPath, dstPath, states)
 
  local mSrcPath = stripVirgule(srcPath)
  local mDstPath = stripVirgule(dstPath)

  if checkFolderExist(mSrcPath) then --文件夹
    local mSrcPathLower = string.lower(mSrcPath.."/")
    local mDstPathLower = string.lower(mDstPath.."/")
    local mSrcPathLen = string.len(mSrcPathLower)
    local mDstPathLen = string.len(mDstPathLower)
    --移动到同路径或子路径则返回失败
    if mDstPathLen >= mSrcPathLen and string.sub(mDstPathLower, 1, mSrcPathLen) == mSrcPathLower then
      return -2
    end
  end

	return moveFile(mSrcPath, mDstPath, states)

end

--__extractFile = files.extract
----------------解压文件-----------------
function extractFile(srcPath, dstPath, states)
	


end

-----------------文件读取-------------------
function readFile(path)
 
  local input = io.open(path, "rb")
  if input == nil then
    return nil
  end
  local data = input:read("*all")

  return data

end 

-----------------文件读取行-------------------
function readLine(path)
 
  local input = io.open(path, "rb")
  if input == nil then
    return nil
  end
  local list = {}
  for line_str in input:lines() do
    table.insert(list, line_str)
  end
  input:close()

  return list

end 

-------------写入数据到文件------------
function writeFile(path, data)
 
  local output = io.open(path, "wb+")
  if output == nil then
    return -1
  end
  output:write(data)
  output:flush()
  output:close()

  return 1

end

-------------写入数据列表到文件------------
function writeLines(path, strs)
 
  local output = io.open(path, "wb+")
  if output == nil then
    return -1
  end

  local dataLen = #strs
  for i = 1, dataLen do
    local line_str = strs[i]
    if i < dataLen then
      line_str = line_str.."\n"
    end
    output:write(line_str)
    output:flush()
  end
  output:close()

  return 1

end

-----------------分割字符串-------------------
function splitString(list, str, separator)
	
	if str == nil then
		return -1
	end
  local str_len = #str
  if str_len < 1 then
    return
  end
  local sepLen = #separator
  local index = 1
  while true do
    local last = string.find(str, separator, index)
    if not last then
      table.insert(list, string.sub(str, index, str_len))
      break
    end
    table.insert(list, string.sub(str, index, last-1))
    index = last + sepLen
  end
  
  return 1
  
end

------------获取文件换行符位置列表----------------------
function getFileLinebreaks(list, path)

  local input = io.open(path, "rb")
  if input == nil then
    return -1
  end

  local size = input:seek("end")
  local read_pos = 0
  local read_bytes = 512

  input:seek("set")

  while true do
    local read_data = input:read(read_bytes)
    if read_data == nil then
      table.insert(list, size + 1)
      break
    end
    local find_pos = 1
    while true do
      local linebreak_pos = string.find(read_data, "\n", find_pos)
      if linebreak_pos == nil then
        break
      end
      table.insert(list, read_pos + linebreak_pos)
      find_pos = linebreak_pos + 1
    end
    read_pos += #read_data
  end
  input:close()

  return 1

end

----------------按宽度断行字符串----------------------
function breakStringByWidth(list, str, limit_width)
 
  if str == nil then
    return -1
  end

  local str_len = #str
  local line_str = ""
  local read_width = 0
  local read_stop_pos = 0
  local i = 1
  while i <= str_len do
		if not line_str then
			line_str = ""
		end
    local curByte = string.byte(str, i)
    local byte_count = 1
    if curByte > 0 and curByte <= 127 then
      byte_count = 1
    elseif curByte >= 192 and curByte <= 223 then
      byte_count = 2
    elseif curByte >= 224 and curByte <= 239 then
      byte_count = 3
    elseif curByte >= 240 and curByte <= 247 then
      byte_count = 4
    end
    read_stop_pos = i + byte_count - 1
    if read_stop_pos > str_len then
			read_stop_pos = str_len
    end
    while true do
			local c = string.sub(str, i, read_stop_pos)
			if c == "\r" and read_stop_pos < str_len then
				local next_pos = read_stop_pos + 1
				local c_next = string.byte(str, next_pos, next_pos)
				if c_next == "\n" then
					local linebreak_str = c..c_next
					read_stop_pos = next_pos
					local line_info = {text = line_str, linebreak = linebreak_str}
					table.insert(list, line_info)
					line_str = nil
					read_width = 0
					break
				end
			elseif c == "\n" then
				local linebreak_str = c
				local line_info = {text = line_str, linebreak = linebreak_str}
				table.insert(list, line_info)
				line_str = nil
				read_width = 0
				break
			end
			local c_width = screen.textwidth(c)
			read_width = read_width + c_width
			if read_width > limit_width then
				local line_info = {text = line_str, linebreak = nil}
				table.insert(list, line_info)
				line_str = c
				read_width = c_width
			else
				line_str = line_str..c
			end
			break
		end
    i = read_stop_pos + 1
  end
	if line_str then
		local line_info = {text = line_str, linebreak = nil}
		table.insert(list, line_info)
	end

  return 1
 
end

------------------更新列表显示信息---------------------
function refreshListStates(top, pos, limit, listLen)

  if not listLen or listLen < 1 then
    return top, listLen, pos
  end

  if pos then
    --获取光标序号
    if pos < 1 then
      pos = 1
    elseif pos > listLen then
      pos = listLen
    end
    --获取顶部序号
    if top > pos then
      top = pos
    elseif top < pos - (limit - 1) then
      top = pos - (limit - 1)
    end 
  else
    --获取顶部序号
    if top > listLen then
      top = listLen 
    end
  end
  local top_to_bottom = listLen - (top - 1)
  if top_to_bottom < limit then
    top = listLen - (limit - 1)
  end
  if top < 1 then
    top = 1
  end
  --获取底部序号
  local bottom = listLen
  if bottom > top + (limit - 1) then
    bottom = top + (limit - 1)
  end

  return top, bottom, pos

end

------------------更新列表显示信息2---------------------
function refreshListStates2(top, pos, limit, listLen)

  if not listLen or listLen < 1 then
    return top, listLen, pos
  end

  if pos then
    --获取光标序号
    if pos < 1 then
      pos = 1
    elseif pos > listLen then
      pos = listLen
    end
    --获取顶部序号
    if top > pos then
      top = pos
    elseif top < pos - (limit - 1) then
      top = pos - (limit - 1)
    end 
  else
    --获取顶部序号
    if top > listLen then
      top = listLen 
    end
  end
  local top_to_bottom = listLen - (top - 1)
  if top_to_bottom < limit then
    top = listLen - (limit - 1)
  end
  if pos and top%2 == 0 then
		if top < pos then
			top += 1
		else
			top -= 1
		end
  end
  if top < 1 then
    top = 1
  end
  --获取底部序号
  local bottom = listLen
  if bottom > top + (limit - 1) then
    bottom = top + (limit - 1)
  end

  return top, bottom, pos

end

----------------更新滚动条显示信息----------------------
function refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
 
  if not listLen or listLen <= limit then
    return 0, 0
  end
	
	--获取滚动条高度
  local line_h = scrollbar_track_h/listLen
  local scrollbar_h = line_h*limit
  if scrollbar_h < 1 then
    scrollbar_h = 1
  end
  --获取滚动条y轴位置
  local scrollbar_y = scrollbar_track_y
  if top > 1 then
    if top == listLen - (limit - 1) then
      scrollbar_y = scrollbar_track_y + scrollbar_track_h - scrollbar_h
    else
      scrollbar_y = scrollbar_track_y + line_h*(top - 1)
    end
  end
  
  return scrollbar_h, scrollbar_y

end

-------------------全选--------------------------
function markAll(list)

  if not list or not #list or #list < 1 then
    return -1
  end

  for i = 1, #list do
    list[i].mark = true
  end

 return 0
 
end

-------------------取消全选--------------------------
function unmarkAll(list)

  if not list or not #list or #list < 1 then
    return -1
  end

  for i = 1, #list do
    list[i].mark = false
  end
  
  return 0
 
end

------------------获取标记数量---------------------
function getMarkCounts(list)
 
  if not list or #list < 1 then
    return -1
  end

  local mark_counts = 0
  for i = 1, #list do
    if list[i].mark then
      mark_counts += 1
    end
  end

  return mark_counts

end







