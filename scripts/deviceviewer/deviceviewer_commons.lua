---------------获取信息列表---------------
function deviceviewer_get_list()

	deviceviewer_list = {
		{text = DEVICEVIEWER_ACCESS, info = DEVICEVIEWER_ACCESS_SAFE_MODE},
		{text = DEVICEVIEWER_LANGUAGE, info = tostring(os.language())},
		{text = DEVICEVIEWER_SWVERSION, info = tostring(os.swversion())},
		{text = DEVICEVIEWER_NICK, info = tostring(os.nick())},
		{text = DEVICEVIEWER_LOGIN, info = tostring(os.login())},
		{text = DEVICEVIEWER_PASSWORD, info = tostring(os.password())},
		{text = DEVICEVIEWER_PSNREGION, info = tostring(os.psnregion())},
		{text = DEVICEVIEWER_IDPS, info = tostring(os.idps())},
		{text = DEVICEVIEWER_PSID, info = tostring(os.psid())},
		{text = DEVICEVIEWER_AID, info = tostring(os.account())},
		{text = DEVICEVIEWER_MAC, info = tostring(os.mac())},
		{text = DEVICEVIEWER_CPU, info = tostring(os.cpu())},
		{text = DEVICEVIEWER_BUS, info = tostring(os.busclock())},
		{text = DEVICEVIEWER_GPU, info = tostring(os.gpuclock())},
		{text = DEVICEVIEWER_XBAR, info = tostring(os.crossbarclock())},
		{text = DEVICEVIEWER_PARTITION, info = DEVICEVIEWER_PARTITION_NO_INFO},
	}
	if os.access() == 1 then
		deviceviewer_list[1].info = DEVICEVIEWER_ACCESS_UNSAFE_MODE
	end
	local devuxo = os.devinfo("ux0:")
	if devuxo then
		deviceviewer_list[16].info = string.format(DEVICEVIEWER_PARTITION_INFO, getSizeFormat(devuxo.free), getSizeFormat(devuxo.max))
	end
	
end

---------------保存信息---------------
function save_ident()

	WaitDialog(WAIT_EXECUTING)
 
	local filePath = "ux0:data/ident.txt"
	local output = io.open(filePath, "w+")
	if output == nil then
		return MessageDialog(TIPS, DEVICEVIEWER_SAVE_FAILED, BACK)
	end
	for i = 1, #deviceviewer_list do
		output:write(deviceviewer_list[i].text.." "..deviceviewer_list[i].info.."\n")
	end
	output:close()
 
	MessageDialog(TIPS, DEVICEVIEWER_SAVE_COMPLETED, BACK)
 
end










