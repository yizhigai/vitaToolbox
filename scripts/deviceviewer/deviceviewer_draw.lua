function deviceviewer_draw()
	
	local math_func = {}

	--按键说明
	local button_texts = {
		DEVICEVIEWER_BUTTON_SAVE,
		DEVICEVIEWER_BUTTON_BACK,
	}

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local limit = 18
	local top = 1
	local bottom = limit
	local list_x = padding_left --x轴
	local list_y = STATUS_BAR_HEIGHT + padding_top --y轴
	local title_x = list_x --x轴
	local info_x = 170 --x轴
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	local need_refresh = true

	-----------------构画主界面---------------------	
	function math_func.draw()
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		local listLen = #deviceviewer_list
		if need_refresh then
			top, bottom, _ = refreshListStates(top, nil, limit, listLen)
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
			need_refresh = false
		end
		--列表
		local mList_y = list_y
		for i = top, bottom do
			screen.print(title_x, mList_y, deviceviewer_list[i].text, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
			screen.print(info_x, mList_y, deviceviewer_list[i].info, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
			mList_y += FONT_LINE_HEIGHT_DEFAULT
		end
		
		--滚动条
		if listLen > limit then
			draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
			draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
		end

		--顶部状态栏
		top_status_bar_draw(DEVICE_VIEWER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()

		if buttons.up or buttons.analogly < -60 then --↑键设
			if top > 1 then
				top -= 1
				need_refresh = true
			end      
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if top < #deviceviewer_list - (limit - 1) then
				top += 1
				need_refresh = true
			end
		end
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end

		if buttons.triangle then --△键
			local state = MessageDialog(TIPS, DEVICEVIEWER_SAVE_READY, CANCEL, POSITIVE)
			if state == 1 then
				save_ident()
			end
		end
	end
		
	return math_func
	
end













