DIALOG_WIDTH = 0 --弹窗宽度最大值
DIALOG_HEIGHT = 0 --弹窗高度度最大值

DIALOG_LINES_MIN = 4 --弹窗文字行数最小限制
DIALOG_LINES_MAX = 16 --弹窗文字行数最大限制

MESSAGE_DIALOG_LINE_MARGIN = 5 --警示弹窗文字行间隔
MESSAGE_DIALOG_LINE_HEIGHT = FONT_HEIGHT_DEFAULT + MESSAGE_DIALOG_LINE_MARGIN --警示弹窗文字行高

MENU_DIALOG_LINE_MARGIN = 7 --菜单弹窗列表行间隔
MENU_DIALOG_LINE_HEIGHT = FONT_HEIGHT_DEFAULT + MENU_DIALOG_LINE_MARGIN --菜单弹窗文字行高

WAIT_DIALOG_WIDTH = 580 --等待弹窗宽度最小值
PROGRESS_DIALOG_WIDTH = 580  --进度弹窗宽度
MESSAGE_DIALOG_WIDTH = 580 --信息弹窗宽度最
MENU_DIALOG_WIDTH_MIN = 400 --菜单弹窗宽度最小值

DIALOG_INCREMENT = 40 --弹窗渐变值

DIALOG_TYPE = __DIALOG_IS_CLOSING

----------------开始弹窗------------
function initDialog()
	
	local dialog_longer_max = DIALOG_WIDTH
	if DIALOG_WIDTH < DIALOG_HEIGHT then
		dialog_longer_max = DIALOG_HEIGHT
	end
	local dialog_width = 0
	local dialog_height = 0
	local dialog_longer = 0

	while true do
		if dialog_longer < dialog_longer_max - DIALOG_INCREMENT then
			if dialog_longer_max == DIALOG_WIDTH then
				dialog_width = dialog_width + DIALOG_INCREMENT
				dialog_height = tonumber(string.format("%d", DIALOG_HEIGHT*(dialog_width/DIALOG_WIDTH))	)
				dialog_longer = dialog_width
			else
				dialog_height = dialog_height + DIALOG_INCREMENT
				dialog_width = tonumber(string.format("%d", DIALOG_WIDTH*(dialog_height/DIALOG_HEIGHT))	)
				dialog_longer = dialog_height
			end
		else
			return
		end

		MATH_FUNC[#MATH_FUNC].draw()
		
		local x = string.format("%d", (SCREEN_WIDTH - dialog_width)/2) --弹窗x轴
		local y = string.format("%d", (SCREEN_HEIGHT - dialog_height)/2) --弹窗y轴
		draw.fillrect(x, y, dialog_width, dialog_height, COLOR_DIALOG_TRACK) --弹窗区域
		draw.rect(x, y, dialog_width, dialog_height, COLOR_DIALOG_STROKE) --弹窗描边
		
		screen.flip()
	end
		 
end

----------------结束弹窗------------
function dismissDialog()
 
	local dialog_longer_max = DIALOG_WIDTH
	if DIALOG_WIDTH < DIALOG_HEIGHT then
		dialog_longer_max = DIALOG_HEIGHT
	end
	local dialog_width = DIALOG_WIDTH
	local dialog_height = DIALOG_HEIGHT
	local dialog_longer = dialog_longer_max
	
	local increment_times = 0
	while true do
		if dialog_longer > DIALOG_INCREMENT and increment_times < 5 then
			increment_times += 1
			if dialog_longer_max == DIALOG_WIDTH then
				dialog_width = dialog_width - DIALOG_INCREMENT
				dialog_height = tonumber(string.format("%d", DIALOG_HEIGHT*(dialog_width/DIALOG_WIDTH))	)
				dialog_longer = dialog_width
			else
				dialog_height = dialog_height - DIALOG_INCREMENT
				dialog_width = tonumber(string.format("%d", DIALOG_WIDTH*(dialog_height/DIALOG_HEIGHT))	)
				dialog_longer = dialog_height
			end
		else
			DIALOG_WIDTH = 0
			DIALOG_HEIGHT = 0
			DIALOG_TYPE = __DIALOG_IS_CLOSING
			MATH_FUNC[#MATH_FUNC].draw()
			screen.flip()
			return
		end

		MATH_FUNC[#MATH_FUNC].draw()
		
		local x = string.format("%d", (SCREEN_WIDTH - dialog_width)/2) --弹窗x轴
		local y = string.format("%d", (SCREEN_HEIGHT - dialog_height)/2) --弹窗y轴
		draw.fillrect(x, y, dialog_width, dialog_height, COLOR_DIALOG_TRACK) --弹窗区域
		draw.rect(x, y, dialog_width, dialog_height, COLOR_DIALOG_STROKE) --弹窗描边
		
		screen.flip()
	end
		 
end

--------------等待弹窗------------
function WaitDialog(msg)
	
	local msgList = {}

	local padding_top = 30	--弹窗内上间距
	local padding_left = 30	--弹窗内左间距
	--信息
	local msg_w = WAIT_DIALOG_WIDTH - padding_left*2
	breakStringByWidth(msgList, msg, msg_w)
	local msgListLen = #msgList
	local msg_h = FONT_HEIGHT_DEFAULT
	if msgListLen > 1 then
		padding_top = 20
		msg_h = (FONT_HEIGHT_DEFAULT + MESSAGE_DIALOG_LINE_MARGIN)*msgListLen - MESSAGE_DIALOG_LINE_MARGIN
	end
	
	DIALOG_WIDTH = padding_left + msg_w + padding_left	--弹窗总宽度
	DIALOG_HEIGHT = padding_top + msg_h + padding_top	--弹窗总高度
	
	if DIALOG_TYPE ~= __WAIT_DIALOG_IS_SHOWING then
		DIALOG_TYPE = __WAIT_DIALOG_IS_SHOWING
	end
	
	local dialog_x = string.format("%d", (SCREEN_WIDTH - DIALOG_WIDTH)/2)	--弹窗x轴
	local dialog_y = string.format("%d", (SCREEN_HEIGHT - DIALOG_HEIGHT)/2)	--弹窗y轴
	local msg_x = dialog_x + padding_left --信息x轴
	local msg_y = dialog_y + padding_top	--信息y轴

	MATH_FUNC[#MATH_FUNC].draw()

	draw.fillrect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_TRACK)
	draw.rect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_STROKE)
	--信息
	local list_y = msg_y
	if msgList and msgListLen > 0 then
		for i = 1, msgListLen do
			msg_x = string.format("%d", (SCREEN_WIDTH - screen.textwidth(msgList[i].text or "", FONT_SIZE_DEFAULT))/2)	--信息x轴
			screen.print(msg_x, list_y, msgList[i].text or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
			list_y = list_y + MESSAGE_DIALOG_LINE_HEIGHT
		end
	end

	screen.flip()
	
end

----------------进度弹窗----------------
function ProgressDialog(title, msg, percent, cancelButton, speed)

	--获取按键文字
	local button_txt = cancelButton
	local button_icon = BUTTON_CANCEL
	if button_assign == 1 then
		button_txt = positiveButton
		button_icon = BUTTON_POSITIVE
	end
	if button_txt then
		button_txt = button_icon..button_txt
	end
	
	--获取构画信息--
	local padding_top = 16	--内上间距
	local padding_left = 30	--内左间距
	--标题
	local title_w = screen.textwidth(title or "", FONT_SIZE_DEFAULT)
	local title_h = FONT_HEIGHT_DEFAULT
	--信息
	local msg_margin_top = 20
	local msg_h = FONT_HEIGHT_DEFAULT
	local msg_w = PROGRESS_DIALOG_WIDTH - padding_left*2
	--进度条
	local progressbar_track_margin_top = 7
	local progressbar_track_h = 6
	local progressbar_track_w = msg_w
	local progressbar_h = progressbar_track_h
	local progressbar_w = (progressbar_track_w/100)*percent
	--进度百分比
	local percent_margin_top = progressbar_track_margin_top
	local percent_h = FONT_HEIGHT_DEFAULT
	--按键
	local button_margin_top = msg_margin_top
	local button_margin_right = 16
	local button_h = FONT_HEIGHT_DEFAULT
	--弹窗总宽度
	DIALOG_WIDTH = padding_left + progressbar_track_w + padding_left
	--x轴--
	local dialog_x = string.format("%d", (SCREEN_WIDTH - DIALOG_WIDTH)/2)
	local dialog_x2 = dialog_x + DIALOG_WIDTH - padding_left
	local title_x = (SCREEN_WIDTH - title_w)/2
	local msg_x = dialog_x + padding_left
	local progressbar_track_x = msg_x
	local progressbar_x = progressbar_track_x
	local percent_x = msg_x
	local speed_x = dialog_x2
	local button_x = dialog_x2 - 50 - screen.textwidth(button_txt or "", FONT_SIZE_DEFAULT)
	--弹窗总高度
	DIALOG_HEIGHT = padding_top + title_h + msg_margin_top + msg_h + progressbar_track_margin_top + progressbar_track_h + percent_margin_top + percent_h + button_margin_top + button_h + padding_top
	--y轴--
	local dialog_y = string.format("%d", (SCREEN_HEIGHT - DIALOG_HEIGHT)/2)
	local title_y = dialog_y + padding_top
	local msg_y = title_y + title_h + msg_margin_top
	local progressbar_track_y = msg_y + msg_h + progressbar_track_margin_top
	local progressbar_y = progressbar_track_y
	local percent_y = progressbar_track_y + progressbar_track_h + percent_margin_top
	local speed_y = percent_y
	local button_y = percent_y + percent_h + button_margin_top

	if DIALOG_TYPE ~= __PROGRESS_DIALOG_IS_SHOWING then
		initDialog()
		DIALOG_TYPE = __PROGRESS_DIALOG_IS_SHOWING
	end

	MATH_FUNC[#MATH_FUNC].draw() --背景

	draw.fillrect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_TRACK)	--弹窗区域
	draw.rect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_STROKE)	--弹窗描边
	
	--标题
	screen.print(title_x, title_y, title or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
	--信息
	screen.print(msg_x, msg_y, msg or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ALEFT, msg_w)
	--进度条
	draw.fillrect(progressbar_track_x, progressbar_track_y, progressbar_track_w, progressbar_track_h, COLOR_PROGRESS_BAR_TRACK) 
	draw.fillrect(progressbar_x, progressbar_y, progressbar_w, progressbar_h, COLOR_PROGRESS_BAR)     
	--百分比
	screen.print(percent_x, percent_y, (percent or 0).."%", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
	--速率
	if speed then
		screen.print(speed_x, speed_y, speed, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ARIGHT)
	end
	--按键
	if button_txt then	
		screen.print(button_x, button_y, button_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
	end

	screen.flip()

	buttons.read()

	if cancelButton and buttons[cancel] then --取消键
		buttons.read()
		dismissDialog()
		return 0
	end
	
	--if percent and percent >= 100 then
		--dismissDialog()
	--end

end

----------------信息弹窗----------------
function MessageDialog(title, msg, cancelButton, positiveButton, negativeButton)

	local msgList = {}
	
	--获取按键文字
	local buttonL_txt = cancelButton
	local buttonR_txt = positiveButton
	local buttonM_txt = negativeButton
	local buttonL_icon = BUTTON_CANCEL
	local buttonR_icon = BUTTON_POSITIVE
	local buttonM_icon = BUTTON_SQUARE
	if button_assign == 1 then
		buttonL_txt = positiveButton
		buttonR_txt = cancelButton
		buttonL_icon = BUTTON_POSITIVE
		buttonR_icon = BUTTON_CANCEL
	end
	if buttonR_txt then	--右键
		buttonR_txt = buttonR_icon..buttonR_txt
	end
	if buttonM_txt then	--中键
		buttonM_txt = buttonM_icon..buttonM_txt
	end
	if buttonL_txt then	--左键
		buttonL_txt = buttonL_icon..buttonL_txt
	end
	
	--获取构画信息--
	local padding_top = 16	--内上间距
	local padding_left = 30	--内左间距
	--标题
	local title_w = screen.textwidth(title, FONT_SIZE_DEFAULT)
	local title_h = FONT_HEIGHT_DEFAULT
	--信息
	local msg_margin_top = 20
	local msg_w = MESSAGE_DIALOG_WIDTH - padding_left*2
	breakStringByWidth(msgList, msg, msg_w)
	local msgListLen = #msgList
	local lineTotal = msgListLen
	if lineTotal < DIALOG_LINES_MIN then
		lineTotal = DIALOG_LINES_MIN
	elseif lineTotal > DIALOG_LINES_MAX then
		lineTotal = DIALOG_LINES_MAX
	end
	local msg_h = (FONT_HEIGHT_DEFAULT + MESSAGE_DIALOG_LINE_MARGIN)*lineTotal - MESSAGE_DIALOG_LINE_MARGIN
	--滚动条
	local scrollbar_track_margin_left = 6
	local scrollbar_track_w = 4
	local scrollbar_track_h = msg_h
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	--按键
	local button_margin_top = msg_margin_top
	local button_margin_right = 20
	local button_h = FONT_HEIGHT_DEFAULT
	
	--弹窗总宽度
	DIALOG_WIDTH = padding_left + msg_w + padding_left
	
	--x轴--
	local dialog_x = string.format("%d", (SCREEN_WIDTH - DIALOG_WIDTH)/2)
	local title_x = (SCREEN_WIDTH - title_w)/2
	local msg_x = dialog_x + padding_left
	local scrollbar_track_x = msg_x + msg_w + scrollbar_track_margin_left
	local scrollbar_x = scrollbar_track_x
	
	local buttonL_w, buttonM_w, buttonR_w = 0, 0, 0
	local buttonL_x, buttonM_x, buttonR_x = 0, 0, 0
	local button_x_original = dialog_x + DIALOG_WIDTH - padding_left
	local button_x = button_x_original
	if buttonR_txt then	--右键
		buttonR_w = screen.textwidth(buttonR_txt, FONT_SIZE_DEFAULT)
		buttonR_x = button_x - buttonR_w
		button_x = buttonR_x - button_margin_right
	end
	if buttonM_txt then	--中键
		buttonM_w = screen.textwidth(buttonM_txt, FONT_SIZE_DEFAULT)
		buttonM_x = button_x - buttonM_w
		button_x = buttonM_x - button_margin_right
	end
	if buttonL_txt then	--左键
		buttonL_w = screen.textwidth(buttonL_txt, FONT_SIZE_DEFAULT)
		if button_x == button_x_original then
			button_x = button_x - 50
		end
		buttonL_x = button_x - buttonL_w
	end

	--弹窗总高度
	DIALOG_HEIGHT = padding_top + title_h + msg_margin_top + msg_h + button_margin_top + button_h + padding_top
	
	--y轴--
	local dialog_y = string.format("%d", (SCREEN_HEIGHT - DIALOG_HEIGHT)/2)	--弹窗y轴
	local title_y = dialog_y + padding_top
	local msg_y = title_y + title_h + msg_margin_top
	local scrollbar_track_y = msg_y
	local scrollbar_y = scrollbar_track_y
	local button_y = msg_y + msg_h + button_margin_top
		
	local top = 1
	local bottom = msgListLen
	local limit = msgListLen
	if limit > lineTotal then
		limit = lineTotal
	end
	local need_refresh = true
	
	DIALOG_TYPE = __MESSAGE_DIALOG_IS_SHOWING
	initDialog()
	
	while true do
		
		MATH_FUNC[#MATH_FUNC].draw() --背景

		draw.fillrect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_TRACK)	--弹窗区域
		draw.rect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_STROKE)	--弹窗描边
	
		if need_refresh then
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, msgListLen, scrollbar_track_h, scrollbar_track_y)
			top, bottom, _ = refreshListStates(top, nil, limit, msgListLen)
			need_refresh = false
		end

		--标题
		screen.print(title_x, title_y, title or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		--信息
		local list_y = msg_y
		if msgList and msgListLen > 0 then
			for i = top, bottom do
				screen.print(msg_x, list_y, msgList[i].text or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
				list_y = list_y + MESSAGE_DIALOG_LINE_HEIGHT
			end

			if msgListLen > limit then --滚动条
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK) --滚动条背景
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR) --滚动条
			end
		end
		--按键
		if buttonL_txt then	--左按键
			screen.print(buttonL_x, button_y, buttonL_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonM_txt then	--中按键
			screen.print(buttonM_x, button_y, buttonM_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonR_txt then	--右按键
			screen.print(buttonR_x, button_y, buttonR_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end

		screen.flip()

		buttons.read()
  
		if buttons.up or buttons.analogly < -60 then --↑键
			if top > 1 then
				top -= 1
				need_refresh = true
			end
		elseif buttons.down or buttons.analogly > 60 then --↓键
			if top < msgListLen - (limit - 1) then
				top += 1
				need_refresh = true
			end
		end

		if cancelButton and buttons[cancel] then --取消键
			buttons.read()
			dismissDialog()
			return 0
		elseif positiveButton and buttons[positive] then --确定键
			buttons.read()
			dismissDialog()
			return 1
		elseif negativeButton and buttons.triangle then	--△键
			buttons.read()
			dismissDialog()
			return 2
		end
		
	end
		
end

----------------列表弹窗----------------
function ItemDialog(title, entries, cancelButton, positiveButton, negativeButton)

	--获取按键文字
	local buttonL_txt = cancelButton
	local buttonR_txt = positiveButton
	local buttonM_txt = negativeButton
	local buttonL_icon = BUTTON_CANCEL
	local buttonR_icon = BUTTON_POSITIVE
	local buttonM_icon = BUTTON_SQUARE
	if button_assign == 1 then
		buttonL_txt = positiveButton
		buttonR_txt = cancelButton
		buttonL_icon = BUTTON_POSITIVE
		buttonR_icon = BUTTON_CANCEL
	end
	if buttonR_txt then	--右键
		buttonR_txt = buttonR_icon..buttonR_txt
	end
	if buttonM_txt then	--中键
		buttonM_txt = buttonM_icon..buttonM_txt
	end
	if buttonL_txt then	--左键
		buttonL_txt = buttonL_icon..buttonL_txt
	end
	
	--获取构画信息--
	local padding_top = 16	--内上间距
	local padding_left = 30	--内左间距
	--标题
	local title_w = screen.textwidth(title, FONT_SIZE_DEFAULT)
	local title_h = FONT_HEIGHT_DEFAULT
	--信息
	local msg_margin_top = 20
	local entriesLen = #entries
	local lineTotal = entriesLen
	if lineTotal < DIALOG_LINES_MIN then
		lineTotal = DIALOG_LINES_MIN
	elseif lineTotal > DIALOG_LINES_MAX then
		lineTotal = DIALOG_LINES_MAX
	end
	local msg_h = (FONT_HEIGHT_DEFAULT + MENU_DIALOG_LINE_MARGIN)*lineTotal - MENU_DIALOG_LINE_MARGIN
	local msg_w = MENU_DIALOG_WIDTH_MIN - padding_left*2
	for i = 1, entriesLen do
		local mWidth = screen.textwidth(entries[i].text or "", FONT_SIZE_DEFAULT)
		if mWidth > msg_w then
			msg_w = mWidth
		end
	end
	--滚动条
	local scrollbar_track_margin_left = 6
	local scrollbar_track_w = 4
	local scrollbar_track_h = msg_h
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	--按键
	local button_margin_top = msg_margin_top
	local button_margin_right = 20
	local button_h = FONT_HEIGHT_DEFAULT

	--弹窗总宽度
	DIALOG_WIDTH = padding_left + msg_w + padding_left
	
	--x轴--
	local dialog_x = string.format("%d", (SCREEN_WIDTH - DIALOG_WIDTH)/2)
	local title_x = (SCREEN_WIDTH - title_w)/2
	local msg_x = dialog_x + padding_left
	local scrollbar_track_x = msg_x + msg_w + scrollbar_track_margin_left
	local scrollbar_x = scrollbar_track_x
	
	local buttonL_w, buttonM_w, buttonR_w = 0, 0, 0
	local buttonL_x, buttonM_x, buttonR_x = 0, 0, 0
	local button_x_original = dialog_x + DIALOG_WIDTH - padding_left
	local button_x = button_x_original
	if buttonR_txt then	--右键
		buttonR_w = screen.textwidth(buttonR_txt, FONT_SIZE_DEFAULT)
		buttonR_x = button_x - buttonR_w
		button_x = buttonR_x - button_margin_right
	end
	if buttonM_txt then	--中键
		buttonM_w = screen.textwidth(buttonM_txt, FONT_SIZE_DEFAULT)
		buttonM_x = button_x - buttonM_w
		button_x = buttonM_x - button_margin_right
	end
	if buttonL_txt then	--左键
		buttonL_w = screen.textwidth(buttonL_txt, FONT_SIZE_DEFAULT)
		if button_x == button_x_original then
			button_x = button_x - 50
		end
		buttonL_x = button_x - buttonL_w
	end

	--弹窗总高度
	DIALOG_HEIGHT = padding_top + title_h + msg_margin_top + msg_h + button_margin_top + button_h + padding_top
	
	--y轴--
	local dialog_y = string.format("%d", (SCREEN_HEIGHT - DIALOG_HEIGHT)/2)	--弹窗y轴
	local title_y = dialog_y + padding_top
	local msg_y = title_y + title_h + msg_margin_top
	local scrollbar_track_y = msg_y
	local scrollbar_y = scrollbar_track_y
	local button_y = msg_y + msg_h + button_margin_top
		
	local limit = entriesLen
	if limit > lineTotal then
		limit = lineTotal
	end
	local top = 1
	local bottom = limit
	local pos = nil
	local tmpPos = 1
	while not entries[tmpPos].enable and tmpPos < entriesLen do
		tmpPos += 1
	end
	if entries[tmpPos].enable then
		pos = tmpPos
	end
	
	local need_refresh = true
	
	DIALOG_TYPE = __MENU_DIALOG_IS_SHOWING
	initDialog()
	
	while true do
	
		MATH_FUNC[#MATH_FUNC].draw() --背景

		draw.fillrect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_TRACK)	--弹窗区域
		draw.rect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_STROKE)	--弹窗描边

		if need_refresh then
			top, bottom, pos = refreshListStates(top, pos, limit, entriesLen)
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, entriesLen, scrollbar_track_h, scrollbar_track_y)
			need_refresh = false
		end
		
		--标题
		screen.print(title_x, title_y, title or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		--信息
		local list_y = msg_y
		if entries and entriesLen > 0 then
			for i = top, bottom do
				local text_color = COLOR_TEXT_DEFAULT
				if pos and i == pos then
					text_color = COLOR_TEXT_POS
				elseif not entries[i].enable then
					text_color = COLOR_TEXT_UNENABLE
				end
				screen.print(msg_x, list_y, entries[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				list_y += MENU_DIALOG_LINE_HEIGHT
			end

			if entriesLen > limit then --滚动条
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK) --滚动条背景
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR) --滚动条
			end
		end
		--按键
		if buttonL_txt then	--左按键
			screen.print(buttonL_x, button_y, buttonL_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonM_txt then	--中按键
			screen.print(buttonM_x, button_y, buttonM_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonR_txt then	--右按键
			screen.print(buttonR_x, button_y, buttonR_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end

		screen.flip()
		
		buttons.read()
  
		if buttons.up or buttons.analogly < -60 then --↑键
			if pos and pos > 1 then
				local tmpPos = pos - 1
				while not entries[tmpPos].enable and tmpPos > 1 do
					tmpPos -= 1
				end
				if entries[tmpPos].enable then
					pos = tmpPos
					need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓键
			if pos and pos < entriesLen then
				local tmpPos = pos + 1
				while not entries[tmpPos].enable and tmpPos < entriesLen do
					tmpPos += 1
				end
				if entries[tmpPos].enable then
					pos = tmpPos
					need_refresh = true
				end
			end
	  
		end
  
		if cancelButton and buttons[cancel] then --取消键
			buttons.read()
			dismissDialog()
			return 0
		elseif positiveButton and buttons[positive] then --确定键
			if pos then
				buttons.read()
				dismissDialog()
				return pos
			end
		elseif negativeButton and buttons.triangle then--△键
			buttons.read()
			dismissDialog()
			return -1
		end
		
	end
		
end

----------------列表弹窗----------------
function InfoDialog(title, entries, cancelButton, positiveButton, negativeButton)

	--获取按键文字
	local buttonL_txt = cancelButton
	local buttonR_txt = positiveButton
	local buttonM_txt = negativeButton
	local buttonL_icon = BUTTON_CANCEL
	local buttonR_icon = BUTTON_POSITIVE
	local buttonM_icon = BUTTON_SQUARE
	if button_assign == 1 then
		buttonL_txt = positiveButton
		buttonR_txt = cancelButton
		buttonL_icon = BUTTON_POSITIVE
		buttonR_icon = BUTTON_CANCEL
	end
	if buttonR_txt then	--右键
		buttonR_txt = buttonR_icon..buttonR_txt
	end
	if buttonM_txt then	--中键
		buttonM_txt = buttonM_icon..buttonM_txt
	end
	if buttonL_txt then	--左键
		buttonL_txt = buttonL_icon..buttonL_txt
	end
	
	--获取构画信息--
	local padding_top = 16	--内上间距
	local padding_left = 30	--内左间距
	--标题
	local title_w = screen.textwidth(title, FONT_SIZE_DEFAULT)
	local title_h = FONT_HEIGHT_DEFAULT
	--信息
	local msg_margin_top = 20
	local entriesLen = #entries
	local lineTotal = entriesLen
	if lineTotal < DIALOG_LINES_MIN then
		lineTotal = DIALOG_LINES_MIN
	elseif lineTotal > DIALOG_LINES_MAX then
		lineTotal = DIALOG_LINES_MAX
	end
	local msg_h = (FONT_HEIGHT_DEFAULT + MENU_DIALOG_LINE_MARGIN)*lineTotal - MENU_DIALOG_LINE_MARGIN
	local msg_w = MENU_DIALOG_WIDTH_MIN - padding_left*2
	for i = 1, entriesLen do
		local mWidth = screen.textwidth(entries[i].text or "", FONT_SIZE_DEFAULT)
		if mWidth > msg_w then
			msg_w = mWidth
		end
	end
	--滚动条
	local scrollbar_track_margin_left = 6
	local scrollbar_track_w = 4
	local scrollbar_track_h = msg_h
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	--按键
	local button_margin_top = msg_margin_top
	local button_margin_right = 20
	local button_h = FONT_HEIGHT_DEFAULT

	--弹窗总宽度
	DIALOG_WIDTH = padding_left + msg_w + padding_left
	
	--x轴--
	local dialog_x = string.format("%d", (SCREEN_WIDTH - DIALOG_WIDTH)/2)
	local title_x = (SCREEN_WIDTH - title_w)/2
	local msg_x = dialog_x + padding_left
	local scrollbar_track_x = msg_x + msg_w + scrollbar_track_margin_left
	local scrollbar_x = scrollbar_track_x
	
	local buttonL_w, buttonM_w, buttonR_w = 0, 0, 0
	local buttonL_x, buttonM_x, buttonR_x = 0, 0, 0
	local button_x_original = dialog_x + DIALOG_WIDTH - padding_left
	local button_x = button_x_original
	if buttonR_txt then	--右键
		buttonR_w = screen.textwidth(buttonR_txt, FONT_SIZE_DEFAULT)
		buttonR_x = button_x - buttonR_w
		button_x = buttonR_x - button_margin_right
	end
	if buttonM_txt then	--中键
		buttonM_w = screen.textwidth(buttonM_txt, FONT_SIZE_DEFAULT)
		buttonM_x = button_x - buttonM_w
		button_x = buttonM_x - button_margin_right
	end
	if buttonL_txt then	--左键
		buttonL_w = screen.textwidth(buttonL_txt, FONT_SIZE_DEFAULT)
		if button_x == button_x_original then
			button_x = button_x - 50
		end
		buttonL_x = button_x - buttonL_w
	end

	--弹窗总高度
	DIALOG_HEIGHT = padding_top + title_h + msg_margin_top + msg_h + button_margin_top + button_h + padding_top
	
	--y轴--
	local dialog_y = string.format("%d", (SCREEN_HEIGHT - DIALOG_HEIGHT)/2)	--弹窗y轴
	local title_y = dialog_y + padding_top
	local msg_y = title_y + title_h + msg_margin_top
	local scrollbar_track_y = msg_y
	local scrollbar_y = scrollbar_track_y
	local button_y = msg_y + msg_h + button_margin_top
		
	local limit = entriesLen
	if limit > lineTotal then
		limit = lineTotal
	end
	local top = 1
	local bottom = limit
	local pos = nil
	local tmpPos = 1
	while not entries[tmpPos].enable and tmpPos < entriesLen do
		tmpPos += 1
	end
	if entries[tmpPos].enable then
		pos = tmpPos
	end
	
	local need_refresh = true
	
	DIALOG_TYPE = __MENU_DIALOG_IS_SHOWING
	initDialog()
	
	while true do
	
		MATH_FUNC[#MATH_FUNC].draw() --背景

		draw.fillrect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_TRACK)	--弹窗区域
		draw.rect(dialog_x, dialog_y, DIALOG_WIDTH, DIALOG_HEIGHT, COLOR_DIALOG_STROKE)	--弹窗描边

		if need_refresh then
			top, bottom, pos = refreshListStates(top, pos, limit, entriesLen)
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, entriesLen, scrollbar_track_h, scrollbar_track_y)
			need_refresh = false
		end
		
		--标题
		screen.print(title_x, title_y, title or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		--信息
		local list_y = msg_y
		if entries and entriesLen > 0 then
			for i = top, bottom do
				local text_color = COLOR_TEXT_DEFAULT
				if pos and i == pos then
					text_color = COLOR_TEXT_POS
				elseif not entries[i].enable then
					text_color = COLOR_TEXT_UNENABLE
				end
				screen.print(msg_x, list_y, entries[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				list_y += MENU_DIALOG_LINE_HEIGHT
			end

			if entriesLen > limit then --滚动条
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK) --滚动条背景
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR) --滚动条
			end
		end
		--按键
		if buttonL_txt then	--左按键
			screen.print(buttonL_x, button_y, buttonL_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonM_txt then	--中按键
			screen.print(buttonM_x, button_y, buttonM_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end
		if buttonR_txt then	--右按键
			screen.print(buttonR_x, button_y, buttonR_txt, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		end

		screen.flip()
		
		buttons.read()
  
		if buttons.up or buttons.analogly < -60 then --↑键
			if pos and pos > 1 then
				local tmpPos = pos - 1
				while not entries[tmpPos].enable and tmpPos > 1 do
					tmpPos -= 1
				end
				if entries[tmpPos].enable then
					pos = tmpPos
					need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓键
			if pos and pos < entriesLen then
				local tmpPos = pos + 1
				while not entries[tmpPos].enable and tmpPos < entriesLen do
					tmpPos += 1
				end
				if entries[tmpPos].enable then
					pos = tmpPos
					need_refresh = true
				end
			end
	  
		end
  
		if cancelButton and buttons[cancel] then --取消键
			buttons.read()
			dismissDialog()
			return 0
		elseif positiveButton and buttons[positive] then --确定键
			if pos then
				buttons.read()
				dismissDialog()
				return pos
			end
		elseif negativeButton and buttons.triangle then--△键
			buttons.read()
			dismissDialog()
			return -1
		end
		
	end
		
end
