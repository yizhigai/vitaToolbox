---------------初始化----------------------
if not SECOND_INTO_EXPLORERR then
	--图标
	fileIcon = image.load("resources/icons/file_icon.png")
	folderIcon = image.load("resources/icons/folder_icon.png")
	textIcon = image.load("resources/icons/text_icon.png")
	imageIcon = image.load("resources/icons/image_icon.png")
	audioIcon = image.load("resources/icons/audio_icon.png")
	archiveIcon = image.load("resources/icons/archive_icon.png")
	sfoIcon = image.load("resources/icons/sfo_icon.png")
	--分区列表
	explorer_partition_infos = {}

	dofile("scripts/explorer/explorer_commons.lua")
	dofile("scripts/explorer/explorer_draw.lua")
	
	explorer_get_partition_list()

	SECOND_INTO_EXPLORERR = true
end

if explorerFunc == nil then
	explorerFunc = explorer_draw()
end
table.insert(MATH_FUNC, explorerFunc)

explorer_copy_list = nil
explorer_list = nil
if #explorer_partition_infos > 0 then
	explorer_get_filelist()
end

