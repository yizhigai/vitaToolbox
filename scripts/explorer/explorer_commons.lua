------------------获取分区列表---------------------
function explorer_get_partition_list()
 
	local partitionList = {
		"ux0:",
		"uma0:",
		"imc0:",
		"xmc0:",
		"gro0:",
		"grw0:",
		"ur0:",
		--"os0:",
		--"pd0:",
		--"sa0:",
		--"tm0:",
		--"ud0:",
		--"vd0:",
		--"vs0:",
	}
	
	for i = 1, #partitionList do
		local partitionPath = partitionList[i]
		if files.exists(partitionPath) then
			local devInfo = {partition_path = partitionPath, parent_path = partitionPath.."/", parent_infos = {}}
			table.insert(explorer_partition_infos, devInfo)
		end
	end

end

------------------获取文件列表---------------------
function explorer_get_filelist()
 
	explorer_list = nil
	local path = explorer_partition_infos[explorer_partition_pos].parent_path
	local fileList = files.list(path)
	if not fileList then
		explorer_need_refresh = true
		return -1
	end
	explorer_list = {}
	table.sort(fileList, function(a, b) return string.lower(a.name) < string.lower(b.name) end)
	local dirInsertOrdinal = 1
	for i = 1, #fileList do
		if fileList[i].directory then
			table.insert(explorer_list, dirInsertOrdinal, fileList[i])
			dirInsertOrdinal += 1
		else
			fileList[i].fsize = getSizeFormat(fileList[i].size)
			table.insert(explorer_list, fileList[i])
		end
	end
	explorer_need_refresh = true
	
	return 1

end

----------------------返回上一层---------------------------
function explorer_cd_up()
	
	local listLen = #explorer_partition_infos[explorer_partition_pos].parent_infos
	explorer_top = explorer_partition_infos[explorer_partition_pos].parent_infos[listLen].top
	explorer_pos = explorer_partition_infos[explorer_partition_pos].parent_infos[listLen].pos
	explorer_partition_infos[explorer_partition_pos].parent_path = explorer_partition_infos[explorer_partition_pos].parent_infos[listLen].parent_path
	table.remove(explorer_partition_infos[explorer_partition_pos].parent_infos, listLen)

	explorer_get_filelist()

end

----------------------打开文件夹---------------------------
function explorer_open_dir()

	local filePath = explorer_list[explorer_pos].path
	local fileName = explorer_list[explorer_pos].name
	local parentPath = explorer_partition_infos[explorer_partition_pos].parent_path
	local parentInfo = {top = explorer_top, pos = explorer_pos, parent_path = parentPath}
	table.insert(explorer_partition_infos[explorer_partition_pos].parent_infos, parentInfo)
	explorer_partition_infos[explorer_partition_pos].parent_path = parentPath..fileName.."/"
	explorer_pos = 1
	explorer_top = 1
	explorer_get_filelist()

end

----------------------打开文件---------------------------
function explorer_open_file()

	local filePath = explorer_list[explorer_pos].path
	local fileName = explorer_list[explorer_pos].name
	local fileExt = explorer_list[explorer_pos].ext
	
	if fileExt == "vpk" then 
		explorer_install_vpk()

	elseif fileExt == "zip" or fileExt == "rar" then
		local state = MessageDialog(TIPS, "这是一个压缩文件，是否要尝试解压？", CANCEL, POSITIVE)
		if state == 1 then
			explorer_extract_file()
		end

	elseif fileExt == "jpg" or fileExt == "png" then
		IMAGEVIEWER_FILE_PATH = filePath
		IMAGEVIEWER_FILE_NAME = fileName
		dofile("scripts/imageviewer/imageviewer.lua")

	elseif fileExt == "mp3" or fileExt == "ogg" or fileExt == "wav" then
	 MessageDialog(TIPS, EXPLORER_MUSIC_TIP, BACK)
	 
	elseif fileExt == "txt" or fileExt == "ini" or fileExt == "sfo" then
		TEXTEDITOR_FILE_PATH = filePath
		TEXTEDITOR_FILE_NAME = fileName
		if fileExt == "sfo" then
		 TEXTEDITOR_OPEN_MODE = __READ
		else
		 TEXTEDITOR_OPEN_MODE = __WRITE
		end
		dofile("scripts/texteditor/texteditor.lua")
		
	else
		local state = MessageDialog(TIPS, EXPLORER_OPEN_UKNOW_FILE_READY, CANCEL, POSITIVE)
		if state == 1 then
			TEXTEDITOR_FILE_PATH = filePath
			TEXTEDITOR_FILE_NAME = fileName
			TEXTEDITOR_OPEN_MODE = __WRITE
			dofile("scripts/texteditor/texteditor.lua")
		end
	end	

end

-----------------复制/剪切---------------------
function explorer_get_copylist()
 
	explorer_copy_list = {}

	local posIsMarked = explorer_list[explorer_pos].mark
	if not posIsMarked then
		table.insert(explorer_copy_list, explorer_list[explorer_pos])
		return
	else
		for i = 1, #explorer_list do
			if explorer_list[i].mark then
				table.insert(explorer_copy_list, explorer_list[i])
				explorer_list[i].mark = false
			end
		end
	end
 
end

------------------粘贴文件---------------------
function explorer_paste_files()
 
	WaitDialog(WAIT_EXECUTING)

	local pasteMode = explorer_copy_mode
	local srcDev = getRootPath(explorer_copy_list[1].path)
	if pasteMode == __MOVE and srcDev ~= explorer_partition_infos[explorer_partition_pos].partition_path then
		local state = MessageDialog(TIPS, EXPLORER_MOVE_DIFFERENCE_DEV_COPY_READY, CANCEL, POSITIVE)
		if state ~= 1 then
			return 0
		end
		pasteMode = __COPY
		WaitDialog(WAIT_EXECUTING)
	end

	lock_home_key()

	local dstPath = explorer_partition_infos[explorer_partition_pos].parent_path
	local states = {
		current = 0,
		total = 0,
	}
	local listLen = #explorer_copy_list
	--检测剩余空间
	if pasteMode == __COPY then
		local countStates = {current = 0, total = listLen}
		for i = 1, listLen do 	--获取总大小
			local mSize = getFileSize(explorer_copy_list[i].path, countStates)
			if mSize == -10 then
				unlock_home_key()
				return
			end
			states.total += mSize
		end
		if not checkFreeSpace(dstPath, states.total) then
			MessageDialog(TIPS, EXPLORER_FREESPACE_NOTENOUGH, BACK)
			return
		end
	elseif pasteMode == __MOVE then
		states.total = listLen
	end
	--复制/移动文件
	local copyLen = 0
	for i = 1, listLen do
		copyLen = i
		local srcPath = explorer_copy_list[i].path
		if pasteMode == __COPY then
			if copyPath(srcPath, dstPath, states) == 0 then
				break
			end
		elseif pasteMode == __MOVE then
			if movePath(srcPath, dstPath, states) == 0 then
				break
			end
		end
	end
	
	explorer_get_filelist()
	for i = 1, #explorer_list do
		local fileName = explorer_list[i].name
		for k = 1, copyLen do
			if string.lower(explorer_copy_list[k].name) == string.lower(fileName) then
				explorer_pos = i
				explorer_list[i].mark = true
				table.remove(explorer_copy_list, k)
				copyLen -= 1
				break
			end
		end
	end
	explorer_copy_list = nil
	explorer_need_refresh = true

	unlock_home_key()
	dismissDialog()
 
end

------------------删除文件---------------------
function explorer_delete_files()

	WaitDialog(WAIT_EXECUTING)

	local deleteList = {}
	local posIsMarked = explorer_list[explorer_pos].mark
	if not posIsMarked then
		table.insert(deleteList, explorer_list[explorer_pos])
		deleteList[#deleteList].pos = explorer_pos
	else
		for i = 1, #explorer_list do
			if explorer_list[i].mark then
				table.insert(deleteList, explorer_list[i])
				deleteList[#deleteList].pos = i
			end
		end
	end
	local delete_counts = #deleteList
	local state = MessageDialog(TIPS, string.format(EXPLORER_DELETE_READY, delete_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local totalCounts = 0
	local countStates = {current = 0, total = delete_counts}
	for i = 1, delete_counts do
		local mCounts = getFileCounts(deleteList[i].path, countStates)
		if mCounts == -10 then
			unlock_home_key()
			return
		end
		totalCounts += mCounts
	end

	local states = {current = 0, total = totalCounts}
	local completed_counts = 0
	local failed_counts = 0
	local tmpPos = explorer_pos
	for i = 1, delete_counts do
		local ret = deletePath(deleteList[i].path, states)
		if ret == 0 then
			explorer_pos = tmpPos
			explorer_get_filelist()
			unlock_home_key()
			return
		elseif ret == 1 then
			completed_counts += 1
			if explorer_pos > deleteList[i].pos then
				tmpPos -= 1
			end
		else
			failed_counts += 1
		end
	end
	explorer_pos = tmpPos
	explorer_get_filelist()
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(EXPLORER_DELETE_COMPLETED, completed_counts, failed_counts), BACK)
 
end

------------------重命名---------------------
function explorer_rename_file()
 
	WaitDialog(WAIT_EXECUTING)
	
	local originalPath = explorer_list[explorer_pos].path
	local originalName = explorer_list[explorer_pos].name
	local newName = osk.init(EXPLORER_RENAME, originalName)
	if not newName or newName == originalName then
		dismissDialog()
		return
	end
	local newPath = getParentPath(originalPath)..newName
	if files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_RENAME_FAILED_ALREADY_EXISTS, BACK) 
		return
	end
	files.rename(originalPath, newName)
	if not files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_RENAME_FAILED, BACK)
		return
	end

	explorer_get_filelist()
	for i = 1, #explorer_list do
		if newName == explorer_list[i].name then
			explorer_pos = i
			explorer_list[i].mark = true
			explorer_need_refresh = true
			break
		end
	end
	dismissDialog()
  
end

------------------新建文件---------------------
function explorer_create_file()
 
	WaitDialog(WAIT_EXECUTING)

	local newName = osk.init(EXPLORER_CREATE_FILE, "")
	if not newName or newName == "" then
		dismissDialog()
		return
	end
	local newPath = explorer_partition_infos[explorer_partition_pos].parent_path..newName
	if files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_CREATE_FILE_FAILED_ALREADY_EXISTS, BACK)
		return
	end
	createFile(newPath)
	if not files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_CREATE_FILE_FAILED, BACK)
		return
	end

	explorer_get_filelist()
	for i = 1, #explorer_list do
		if newName == explorer_list[i].name then
			explorer_pos = i
			explorer_list[i].mark = true
			explorer_need_refresh = true
			break
		end
	end
	dismissDialog()

end

------------------新建文件夹---------------------
function explorer_create_folder()

	WaitDialog(WAIT_EXECUTING)

	local newName = osk.init(EXPLORER_CREATE_FOLDER, "")
	if not newName or newName == "" then
		dismissDialog()
		return
	end
	local newPath = explorer_partition_infos[explorer_partition_pos].parent_path..newName
	if files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_CREATE_FOLDER_FAILED_ALREADY_EXISTS, BACK)
		return
	end
	createFolder(newPath)
	if not files.exists(newPath) then
		MessageDialog(TIPS, EXPLORER_CREATE_FOLDER_FAILED, BACK)
		return
	end

	explorer_get_filelist()
	for i = 1, #explorer_list do
		if newName == explorer_list[i].name then
			explorer_pos = i
			explorer_list[i].mark = true
			explorer_need_refresh = true
			break
		end
	end
	dismissDialog()

end

----------------安装游戏压缩包---------------------
function explorer_install_vpk()
 
	WaitDialog(WAIT_EXECUTING)
	
	local path = explorer_list[explorer_pos].path
	local uxDev = "ux0:"
	--如果没有找到ux0盘符，则返回
	if not files.exists(uxDev) then
		MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_NO_UX_DEV, BACK)
		return
	end
	--获取游戏信息
	local gameInfo = game.info(path)
	if not gameInfo or not gameInfo.TITLE_ID then
		MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_GET_GAMEINFO_FAILED, BACK)
		return
	end
	local version = gameInfo.APP_VER
	local gameId = gameInfo.TITLE_ID
	local gameTitle = gameInfo.TITLE
	if gameTitle then
		gameTitle = gameTitle:gsub("\n", " ")
	end
	local state = MessageDialog(EXPLORER_INSTALL_FOLDER_GAME_INFO_TITLE, string.format(EXPLORER_INSTALL_FOLDER_GAME_INFO_MESSAGE, gameTitle or "", version or "", gameId or ""), CANCEL, INSTALL)
	if state == 0 then
		return
	end
	--安装游戏
	INSTALL_STEP = 0
	INSTALL_GAME_TITLE = gameTitle
	if gameId and gameId == os.titleid() then
		MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_CONT_INSTALL_SELF, BACK)
		return
	end
	
	lock_home_key()
	if ProgressDialog(NOW_INSTALLING, INSTALL_GAME_TITLE, 0, CANCEL, nil) == 0 then
		return
	end
	
	local retStr = ""
	local ret = game.install(path, false)
	if ret == 1 then
		if ProgressDialog(NOW_INSTALLING, INSTALL_GAME_TITLE, 100, CANCEL, nil) == 0 then
			return
		end
		retStr = EXPLORER_INSTALL_VPK_COMPLETED
	else
		if INSTALL_STEP < 3 then
			retStr = EXPLORER_INSTALL_VPK_FREESPACE_NOTENOUGH
		else
			retStr = EXPLORER_INSTALL_VPK_FAILED
		end
	end
	
	unlock_home_key()
	dismissDialog()
	
	if INSTALL_CANCEL then
		return
	end
	MessageDialog(TIPS, string.format(retStr, gameTitle), BACK)
 
end

----------------安装游戏文件夹---------------------
function explorer_install_dir()
 
	WaitDialog(WAIT_EXECUTING)

	local srcDev = explorer_partition_infos[explorer_partition_pos].partition_path
	if not files.exists("ux0:") then --如果没有找到ux0分区则返回
		MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_NO_UX_DEV, BACK)
		return
	end
	if srcDev ~= "ux0:" then --如果安装包不在ux0分区则返回
		MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_NOT_IN_UX, BACK)
		return
	end

	local installList = {}
	local install_counts = 1
	local posIsMarked = explorer_list[explorer_pos].mark
	if not posIsMarked then
		local installPath = explorer_list[explorer_pos].path
		local sfoPath = installPath.."/sce_sys/param.sfo"
		local gameInfo = game.info(sfoPath)
		if not gameInfo or not gameInfo.TITLE_ID then
			MessageDialog(TIPS, EXPLORER_INSTALL_FOLDER_GET_GAMEINFO_FAILED, BACK)
			return
		end
		local version = gameInfo.APP_VER
		local gameId = gameInfo.TITLE_ID
		local gameTitle = gameInfo.TITLE
		if gameTitle then
			gameTitle = gameTitle:gsub("\n", " ")
		end
		local state = MessageDialog(EXPLORER_INSTALL_FOLDER_GAME_INFO_TITLE, string.format(EXPLORER_INSTALL_FOLDER_GAME_INFO_MESSAGE, gameTitle or "", version or "", gameId or ""), CANCEL, INSTALL)
		if state == 0 then
			return
		end
		table.insert(installList, explorer_list[explorer_pos])
		installList[#installList].pos = explorer_pos
	else
		for i = 1, #explorer_list do
			if explorer_list[i].mark and explorer_list[i].directory then
				table.insert(installList, explorer_list[i])
				installList[#installList].pos = i
			end
		end
		install_counts = #installList
		local state = MessageDialog(TIPS, string.format(EXPLORER_INSTALL_FOLDER_READY, install_counts), CANCEL, POSITIVE)
		if state == 0 then
			return
		end
	end

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local install_counts = #installList
	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	local tmpPos = explorer_pos
	for i = 1, install_counts do
		if ProgressDialog(NOW_INSTALLING, installList[i].name, percent, CANCEL) == 0 then
			break
		end
		local ret = install_game_dir(installList[i].path)
		if ret == 1 then
			if explorer_pos > installList[i].pos then
				tmpPos -= 1
			end
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/install_counts)*100)
		if ProgressDialog(NOW_INSTALLING, installList[i].name, percent, CANCEL) == 0 then
			break
		end		
	end
	explorer_pos = tmpPos
	explorer_get_filelist()
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(EXPLORER_INSTALL_FOLDER_COMPLETED, completed_counts, failed_counts), BACK)

end

------------------导出多媒体文件---------------------
function explorer_export_multimedia()

	WaitDialog(WAIT_EXECUTING)

	local exportList = {}
	local posIsMarked = explorer_list[explorer_pos].mark
	if not posIsMarked then
		table.insert(exportList, explorer_list[explorer_pos])
	else
		for i = 1, #explorer_list do
			if explorer_list[i].mark then
				local fileExt = explorer_list[i].ext
				if fileExt == "png" or fileExt == "jpg" or fileExt == "bmp" or fileExt == "gif" or fileExt == "mp3" or fileExt == "mp4" then
					table.insert(exportList, explorer_list[i])
				end
			end
		end
	end
	local export_counts = #exportList
	local state = MessageDialog(TIPS, string.format(EXPLORER_EXPORT_MULTIMEDIA_READY, export_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	for i = 1, export_counts do
		if ProgressDialog(NOW_EXPORTING, exportList[i].name, percent, CANCEL) == 0 then
			break
		end
		local retStr = ""
		local ret = files.export(exportList[i].path)
		if ret == 1 then
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/export_counts)*100)
		if ProgressDialog(NOW_EXPORTING, exportList[i].name, percent, CANCEL) == 0 then
			break
		end		
	end
	explorer_get_filelist()
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(EXPLORER_EXPORT_MULTIMEDIA_COMPLETED, completed_counts, failed_counts), BACK)

end

------------------属性---------------------
function explorer_properties()

	WaitDialog(WAIT_EXECUTING)
	local filePath = explorer_list[explorer_pos].path
	local fileName = explorer_list[explorer_pos].name
	local fileType = EXPLORER_PROPERTIES_FILE
	if explorer_list[explorer_pos].directory then
		fileType = EXPLORER_PROPERTIES_FOLDER
	end

	local countStates = {current = 0, total = 1}
	local mSize = getFileSize(filePath, countStates)
	if mSize == -10 then
		return
	end
	local fSize = getSizeFormat(mSize)
	dismissDialog()
	
	MessageDialog(EXPLORER_PROPERTIES, string.format(EXPLORER_PROPERTIES_MESSAGE, fileName, fileType, fSize), BACK)

end

------------------解压文件---------------------
function explorer_extract_file()
	
	ProgressDialog(NOW_EXTRACTING, fileName, 0, CANCEL, nil)
	local filePath = explorer_list[explorer_pos].path
	local fileName = explorer_list[explorer_pos].name
	local dstDir = stripExtension(filePath)
	local ret = files.extract(filePath, dstDir)
	explorer_get_filelist()
	local lowerDstDirName = string.lower(getFileName(dstDir))
	for i = 1, #explorer_list do
		local mFileName = explorer_list[i].name
		if string.lower(mFileName) == lowerDstDirName then
			explorer_pos = i
			explorer_list[i].mark = true
			break
		end
	end
	dismissDialog()
	if not EXTRACT_CANCEL and ret ~= 1 then
		MessageDialog(TIPS, "解压文件失败！", BACK)
	end
			
end

----------------------打开菜单---------------------------
function explorer_open_menu()

	local entries = {
		{text = EXPLORER_MARKS_ALL, enable = false},
		{text = EXPLORER_COPY, enable = false},
		{text = EXPLORER_CUT, enable = false},
		{text = EXPLORER_PASTE, enable = false},
		{text = EXPLORER_DELETE, enable = false},
		{text = EXPLORER_RENAME, enable = false},
		{text = EXPLORER_CREATE_FILE, enable = false},
		{text = EXPLORER_CREATE_FOLDER, enable = false},
		{text = EXPLORER_INSTALL_FOLDER, enable = false},
		{text = EXPLORER_EXPORT_MULTIMEDIA, enable = false},
		{text = EXPLORER_PROPERTIES, enable = false},
		{text = "", enable = false},
		{text = EXPLORER_EXIT, enable = true},
	}
	
	local listLen = 0
	local mark_counts = 0
	if explorer_list then
		if explorer_copy_list and #explorer_copy_list > 0 then
			entries[4].enable = true --粘贴
		end		
		entries[7].enable = true --新建文件
		entries[8].enable = true --新建文件夹
		listLen = #explorer_list
		if listLen > 0 then
			local filePath = explorer_list[explorer_pos].path
			mark_counts = getMarkCounts(explorer_list)
			if mark_counts == listLen then
				entries[1].text = EXPLORER_UNMARKS_ALL
			end
			entries[1].enable = true
			entries[2].enable = true
			entries[3].enable = true
			entries[5].enable = true
			entries[6].enable = true
			if explorer_list[explorer_pos].directory then
				if checkFileExist(filePath.."/eboot.bin") and checkFileExist(filePath.."/sce_sys/param.sfo") then
					entries[9].enable = true
				end
			else
				local fileExt = explorer_list[explorer_pos].ext
				if fileExt == "png" or fileExt == "jpg" or fileExt == "bmp" or fileExt == "gif" or fileExt == "mp3" or fileExt == "mp4" then
					entries[10].enable = true
				end
			end
			entries[11].enable = true
		end
	end

	local sel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if sel == 1 then --全选
		if mark_counts < listLen then
			markAll(explorer_list)
		else
			unmarkAll(explorer_list)
		end
		
	elseif sel == 2 then --复制
		explorer_copy_mode = __COPY
		explorer_get_copylist()
		
	elseif sel == 3 then --剪切
		explorer_copy_mode = __MOVE
		explorer_get_copylist()
		
	elseif sel == 4 then --粘贴
		explorer_paste_files()
		
	elseif sel == 5 then --删除
		explorer_delete_files()
		
	elseif sel == 6 then --重命名
		explorer_rename_file()
		
	elseif sel == 7 then --新建文件
		explorer_create_file()
		
	elseif sel == 8 then --新建文件夹
		explorer_create_folder()
		
	elseif sel == 9 then --安装游戏文件夹
		explorer_install_dir()
		
	elseif sel == 10 then --导出多媒体文件
		explorer_export_multimedia()
		
	elseif sel == 11 then --属性
		explorer_properties()
		
	elseif sel == 13 then --退出
		table.remove(MATH_FUNC, #MATH_FUNC)
	end

end









