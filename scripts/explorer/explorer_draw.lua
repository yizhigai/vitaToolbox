explorer_partition_pos = 1
explorer_top = 1
explorer_pos = 1
explorer_need_refresh = true

function explorer_draw()

	local math_func = {}

	--按键说明
	local button_texts = {
		EXPLORER_BUTTON_MARK,
		EXPLORER_BUTTON_OPEN_MENU,
		EXPLORER_BUTTON_POSITIVE,
		EXPLORER_BUTTON_BACK,
		EXPLORER_BUTTON_SWITCH_PARTITION,
		EXPLORER_BUTTON_REFRESH_LIST,
	}

	local limit = 17
	local bottom = limit
	local pos_old = explorer_pos

	--全局
	local padding_top = 4
	local padding_left = 18
	--文件父路径
	local parentpath_margin_right = 10
	local parentpath_x = padding_left
	local parentpath_y = STATUS_BAR_HEIGHT + padding_top
	--分区容量
	local partitionsize_x = SCREEN_WIDTH - padding_left
	local partitionsize_y = parentpath_y
	--分区列表
	local partitionlist_track_margin_top = padding_top --区域
	local partitionlist_track_padding = 2
	local partitionlist_track_x = parentpath_x
	local partitionlist_track_y = parentpath_y + FONT_HEIGHT_DEFAULT + partitionlist_track_margin_top
	local partitionlist_track_w = SCREEN_WIDTH - padding_left*2
	local partitionlist_track_h = partitionlist_track_padding + FONT_HEIGHT_DEFAULT + partitionlist_track_padding
	local partitionlist_pos_w = nil --光标
	local partitionlist_pos_h = partitionlist_track_h --光标
	local partitionlist_text_margin_top = partitionlist_track_padding --文字
	local partitionlist_text_y = partitionlist_track_y + partitionlist_text_margin_top
	local partitionlist_margin_top = 2 --分割栏
	local partitionlist_w = 1
	local partitionlist_h = partitionlist_track_h - partitionlist_margin_top*2
	local partitionlist_y = partitionlist_track_y + partitionlist_margin_top
	--文件列表
	local list_margin_top = 6
	local list_x = parentpath_x
	local list_y = partitionlist_track_y + partitionlist_track_h + list_margin_top
	local list_w = SCREEN_WIDTH - padding_left*2
	local list_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local icon_x = list_x --图标
	local name_x_default = 42 --文件名
	local name_x = name_x_default
	local name_w = 480
	local size_x = 660 --大小
	local time_x = SCREEN_WIDTH - padding_left --时间
	local mark_padding_top = 3 --标记
	local mark_x = list_x
	local mark_w = list_w
	local mark_h = FONT_HEIGHT_DEFAULT + mark_padding_top*2
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = list_h
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if EXPLORER_BACKGROUND then EXPLORER_BACKGROUND:blit(0, 0) end
		
		if explorer_partition_infos and #explorer_partition_infos > 0 then
			local partition_listLen = #explorer_partition_infos
			--分区大小
			local partitionSizeText = EXPLORER_PARTITION_NO_INFO
			local partitionSizeColor = COLOR_RED
			local partitionInfo = os.devinfo(explorer_partition_infos[explorer_partition_pos].partition_path)
			if partitionInfo then
				partitionSizeText = string.format(EXPLORER_PARTITION_INFO, getSizeFormat(partitionInfo.free), getSizeFormat(partitionInfo.max))
				partitionSizeColor = COLOR_TEXT_POS
			end
			screen.print(partitionsize_x, partitionsize_y, partitionSizeText, FONT_SIZE_DEFAULT, partitionSizeColor, COLOR_BLACK, __ARIGHT)
			--父路径
			local parentpath_w = list_w - screen.textwidth(partitionSizeText) - parentpath_margin_right
			screen.print(parentpath_x, parentpath_y, explorer_partition_infos[explorer_partition_pos].parent_path, FONT_SIZE_DEFAULT, COLOR_GREEN, COLOR_BLACK, __ALEFT, parentpath_w)
			--分区列表
			draw.fillrect(partitionlist_track_x, partitionlist_track_y, partitionlist_track_w, partitionlist_track_h, COLOR_TEXT_TRACK_DEFAULT)
			local partitionlist_pos_x = partitionlist_track_x
			if partitionlist_pos_w == nil then
				partitionlist_pos_w = string.format("%d", (partitionlist_track_w - (partition_listLen - 1)*partitionlist_w)/partition_listLen)
			end
			for i = 1, partition_listLen do
				if i == explorer_partition_pos then
					draw.fillrect(partitionlist_pos_x, partitionlist_track_y, partitionlist_pos_w, partitionlist_pos_h, COLOR_TEXT_TRACK_POS)
				end
				if i > 1 then
					local partitionlist_x = partitionlist_pos_x - partitionlist_w
					draw.fillrect(partitionlist_x, partitionlist_y, partitionlist_w, partitionlist_h, COLOR_MARGIN)
				end
				local partitionlist_text_x = partitionlist_pos_x + partitionlist_pos_w/2
				screen.print(partitionlist_text_x, partitionlist_text_y, explorer_partition_infos[i].partition_path, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, 0x0, __ACENTER)
				partitionlist_pos_x += (partitionlist_pos_w + partitionlist_w)
			end
			
			if explorer_list then
				if #explorer_list > 0 then
					local listLen = #explorer_list
					if explorer_need_refresh then
						explorer_top, bottom, explorer_pos = refreshListStates(explorer_top, explorer_pos, limit, listLen)
						scrollbar_h, scrollbar_y = refreshScrollbarStates(explorer_top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
						explorer_need_refresh = false
					end
					if pos_old ~= explorer_pos then
						pos_old = explorer_pos
						name_x = name_x_default
					end
					--文件列表
					local mList_y = list_y
					for i = explorer_top, bottom do
						--标记
						if explorer_list[i].mark then
							local mark_y = mList_y - mark_padding_top
							draw.fillrect(mark_x, mark_y, mark_w, mark_h, COLOR_MARK) 
						end

						local text_color = COLOR_TEXT_DEFAULT
						if i == explorer_pos then
							text_color = COLOR_TEXT_POS
						end
						--文件图标
						local icon = fileIcon
						if explorer_list[i].directory then
							icon = folderIcon
						else
							local fileExt = explorer_list[i].ext
							if fileExt == "txt" or fileExt == "ini" then
								icon = textIcon
							elseif fileExt == "jpg" or fileExt == "png" then
								icon = imageIcon
							elseif fileExt == "mp3" or fileExt == "wave" then
								icon = audioIcon
							elseif fileExt == "zip" or fileExt == "rar" or fileExt == "vpk" then
								icon = archiveIcon
							elseif fileExt == "sfo" then
								icon = sfoIcon
							end
						end
						image.blit(icon, icon_x, mList_y)
						--文件名
						local fileName = explorer_list[i].name
						if  i == explorer_pos and screen.textwidth(explorer_list[i].name or "") > name_w then
							name_x = screen.print(name_x, mList_y, explorer_list[i].name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, name_w)
						else
							screen.print(name_x_default, mList_y, explorer_list[i].name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, name_w)
						end
						--文件大小
						screen.print(size_x, mList_y, explorer_list[i].fsize or EXPLORER_FOLDER, FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ARIGHT)
						--文件日期
						screen.print(time_x, mList_y, explorer_list[i].mtime or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ARIGHT)
						mList_y += FONT_LINE_HEIGHT_DEFAULT
					end
					--滚动条
					if listLen > limit then
						draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
						draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
					end
				else
					screen.print(list_x, list_y, EXPLORER_NO_FILE, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK) --没有文件
				end
			else
				screen.print(list_x, list_y, EXPLORER_LIST_ERROR, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK) --获取文件列表出错
			end
		else
			screen.print(parentpath_x, parentpath_y, EXPLORER_NO_PARTITION, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK) --没有分区
		end

		--顶部状态栏
		top_status_bar_draw(EXPLORER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()

		if buttons.start then
			explorer_get_filelist(explorer_partition_infos[explorer_partition_pos].parent_path)
		end --start键
				
		if buttons.up or buttons.analogly < -60 then --↑键设
			if explorer_list and #explorer_list > 0 then
				if explorer_pos > 1 then
					explorer_pos -= 1
					explorer_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if explorer_list and #explorer_list > 0 then
				if explorer_pos < #explorer_list then
					explorer_pos += 1
					explorer_need_refresh = true
				end
			end
		end

		if buttons.left then --←键设置
			if explorer_partition_infos and #explorer_partition_infos > 0 then
				local tmpPos = explorer_partition_pos - 1
				if tmpPos < 1 then
					tmpPos = #explorer_partition_infos
				end
				if explorer_partition_pos ~= tmpPos then
					explorer_partition_infos[explorer_partition_pos].top = explorer_top
					explorer_partition_infos[explorer_partition_pos].pos = explorer_pos
					explorer_partition_pos = tmpPos
					explorer_top = explorer_partition_infos[explorer_partition_pos].top or 1
					explorer_pos = explorer_partition_infos[explorer_partition_pos].pos or 1
					explorer_get_filelist(explorer_partition_infos[explorer_partition_pos].parent_path)
				end
			end
		elseif buttons.right then --→键设置
			if explorer_partition_infos and #explorer_partition_infos > 0 then
				local tmpPos = explorer_partition_pos + 1
				if tmpPos > #explorer_partition_infos then
					tmpPos = 1
				end
				if explorer_partition_pos ~= tmpPos then
					explorer_partition_infos[explorer_partition_pos].top = explorer_top
					explorer_partition_infos[explorer_partition_pos].pos = explorer_pos
					explorer_partition_pos = tmpPos
					explorer_top = explorer_partition_infos[explorer_partition_pos].top or 1
					explorer_pos = explorer_partition_infos[explorer_partition_pos].pos or 1
					explorer_get_filelist(explorer_partition_infos[explorer_partition_pos].parent_path)
				end
			end
		end
	
		if buttons[cancel] then --取消键
			if explorer_partition_infos[explorer_partition_pos].parent_infos and #explorer_partition_infos[explorer_partition_pos].parent_infos > 0 then
				explorer_cd_up()
			else
				table.remove(MATH_FUNC, #MATH_FUNC)
			end
		end

		if buttons.square then --□键
			if explorer_list and #explorer_list > 0 then
				if explorer_list[explorer_pos].mark then
					explorer_list[explorer_pos].mark = false
				else
					explorer_list[explorer_pos].mark = true
				end
			end
		end
	
		if buttons.triangle then --△键
			explorer_open_menu()
		end
		
		if buttons[positive] then --确定键
			if explorer_list and #explorer_list > 0 then
				if explorer_list[explorer_pos].directory then
					explorer_open_dir()
				else
					explorer_open_file()
				end
			end
		end
	end
		
	return math_func
	
end

