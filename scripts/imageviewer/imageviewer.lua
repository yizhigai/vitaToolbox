
---------------初始化---------------
if not SECOND_INTO_IMAGEVIEWER then
	dofile("scripts/imageviewer/imageviewer_commons.lua")
	dofile("scripts/imageviewer/imageviewer_draw.lua")

	SECOND_INTO_IMAGEVIEWER = true
end

IMAGEVIEWER_SHOW_STATUSBAR = true
IMAGEVIEWER_IS_LOADING = true

if imageviewerFunc == nil then
	imageviewerFunc = imageviewer_draw()
end
table.insert(MATH_FUNC, imageviewerFunc)

IMAGEVIEWER_IMAGE = imageviewer_load_image(IMAGEVIEWER_FILE_PATH)

