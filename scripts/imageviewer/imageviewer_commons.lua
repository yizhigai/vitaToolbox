
--------------获取图片-------------------------------
function imageviewer_load_image(path)
	
	IMAGEVIEWER_IS_LOADING = true
	if getFileSize(path) > 150*1024 then
		imageviewerFunc.draw()
		screen.flip()
	end
	
	local img = image.load(path)
	if not img then
		IMAGEVIEWER_IS_LOADING = false
		return nil
	end
	local original_w = image.getw(img)
	local original_h = image.geth(img)
	local image_w = original_w
	local image_h = original_h
	local limit_w = 960
	local limit_h = 544
	--local image_scale = -1
	if image_w > limit_w then
		image_w = limit_w
		image_h = original_h*(image_w/original_w)
		--image_scale = (image_w/original_w)*100
	end
	if image_h > limit_h then
		image_h = limit_h
		image_w = original_w*(image_h/original_h)
		--image_scale = (image_h/original_h)*100
	end
	--if image_scale ~= -1 then
	 --image.scale(img, image_scale)
	--end
	image.resize(img, image_w, image_h)
	IMAGEVIEWER_IMAGE_X = (960 - image_w)/2
	IMAGEVIEWER_IMAGE_Y = (544 - image_h)/2
	
	IMAGEVIEWER_IS_LOADING = false
	
	return img
	
end



