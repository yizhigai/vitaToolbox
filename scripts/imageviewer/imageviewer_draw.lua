
function imageviewer_draw()

	local math_func = {}
	
	--按键说明
	local button_texts = {
		IMAGEVIEWER_BUTTON_HIDE,
		IMAGEVIEWER_BUTTON_BACK,
		IMAGEVIEWER_BUTTON_SWITCH,
	}

	local center_x = SCREEN_WIDTH/2
	local center_y = SCREEN_HEIGHT/2-FONT_HEIGHT_DEFAULT/2

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		draw.fillrect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, COLOR_IMAGEVIEWER_BG)

		--图片
		if IMAGEVIEWER_IMAGE then
			IMAGEVIEWER_IMAGE:blit(IMAGEVIEWER_IMAGE_X, IMAGEVIEWER_IMAGE_Y)
		elseif not IMAGEVIEWER_IS_LOADING then
		 screen.print(center_x, center_y, IMAGEVIEWER_LOAD_IMAGE_FAILED, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK, __ACENTER)
		end
		if IMAGEVIEWER_IS_LOADING then
			screen.print(center_x, center_y, IMAGEVIEWER_LOADING_IMAGE, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ACENTER)
		end

		if IMAGEVIEWER_SHOW_STATUSBAR then
			top_status_bar_draw(IMAGEVIEWER_FILE_NAME)
			bottom_status_bar_draw(button_texts)
		end
		
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.left then --←键
			if explorer_list and #explorer_list > 0 then
				local tmpPos = explorer_pos
				while tmpPos > 1 do
					tmpPos -= 1
					local fileExt = explorer_list[tmpPos].ext
					if fileExt == "jpg" or fileExt == "png" then
						explorer_pos = tmpPos
						IMAGEVIEWER_FILE_PATH = explorer_list[explorer_pos].path
						IMAGEVIEWER_FILE_NAME = explorer_list[explorer_pos].name
						IMAGEVIEWER_IMAGE = imageviewer_load_image(IMAGEVIEWER_FILE_PATH)
						break
					end
				end
			end
		elseif buttons.right then --→鍵
			if explorer_list and #explorer_list > 0 then
				local tmpPos = explorer_pos
				while tmpPos < #explorer_list do
					tmpPos += 1
					local fileExt = explorer_list[tmpPos].ext
					if fileExt == "jpg" or fileExt == "png" then
						explorer_pos = tmpPos
						IMAGEVIEWER_FILE_PATH = explorer_list[explorer_pos].path
						IMAGEVIEWER_FILE_NAME = explorer_list[explorer_pos].name
						IMAGEVIEWER_IMAGE = imageviewer_load_image(IMAGEVIEWER_FILE_PATH)
						break
					end
				end
			end
		end
	
		if buttons[cancel] then --取消键
			IMAGEVIEWER_IMAGE = nil
			IMAGEVIEWER_FILE_PATH = nil
			IMAGEVIEWER_FILE_NAME = nil
			if explorer_list then
				explorer_get_filelist()
			end
			table.remove(MATH_FUNC, #MATH_FUNC)
		end
		
		if buttons.square then --□键
			IMAGEVIEWER_SHOW_STATUSBAR = not IMAGEVIEWER_SHOW_STATUSBAR
		end
	end
		
	return math_func
	
end

