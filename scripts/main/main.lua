
--获取状态栏高度
STATUS_BAR_HEIGHT = 12 + FONT_HEIGHT_DEFAULT + 12

dofile("scripts/main/main_commons.lua")
dofile("scripts/main/main_draw.lua")

--自动播放路径为resources/bgm.mp3
playBgmMusic()

MATH_FUNC = {}

if mainFunc == nil then
	mainFunc = main_draw()
end
table.insert(MATH_FUNC, mainFunc)

--检测不安全模式
checkUnsafeMode()

while true do
	MATH_FUNC[#MATH_FUNC].draw()
	screen.flip()
	MATH_FUNC[#MATH_FUNC].ctrl()
end
