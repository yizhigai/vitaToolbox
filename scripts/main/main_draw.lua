main_pos = 1
main_need_refresh = true

function main_draw()

	local math_func = {}

	--选项
	local main_entries = {
		{text = EXPLORER, func = explorerInit, tip = false},
		{text = PSVGAME_MANAGER, func = psvgameManagerInit, tip = false},
		{text = PLUGIN_MANAGER, func = pluginManagerInit, tip = false},
		{text = MUSIC_PLAYER, func = musicPlayerInit, tip = false},
		{text = GAMESD_TOOL, func = gamesdToolInit, tip = false},
		{text = DEVICE_VIEWER, func = deviceViewerInit, tip = false},
		{text = REFRESH_GAME, func = refreshGameInit, tip = false},
		{text = ACCOUNT_MANAGER, func = accountManagerInit, tip = false},
		{text = REBUILD_DATABASE, func = rebuildDatabaseInit, tip = false},
		{text = RELINK_MC, func = relinkMcInit, tip = false},
		{text = REBOOT, func = rebootInit, tip = false},
		{text = CHECK_UPDATER, func = checkUpdateInit, tip = false},
		{text = MENU_MORE, func = menuMoreInit, tip = false},
		{text = ABOUT, func = aboutInit, tip = false},
	}
	--按键说明
	local button_texts = {
		MAIN_BUTTON_SELECT,
		MAIN_BUTTON_START,
		MAIN_BUTTON_POSITIVE,
	}

	local limit = 14
	local top = 1
	local bottom = limit
	local pos_old = main_pos

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local entry_padding_left = 16
	local entry_padding_top = 16
	local entry_margin_bottom = 14
	local entry_w = SCREEN_WIDTH_HALF - padding_left - padding_left/2 --菜单宽度
	local entry_h = entry_padding_top + FONT_HEIGHT_DEFAULT + entry_padding_top --菜单高度
	local entry_y = STATUS_BAR_HEIGHT + padding_top --y轴
	local entry_line_h = entry_h + entry_margin_bottom --菜单行高度
	local entry_text_w = entry_w - entry_padding_left*2 --左菜单x轴
	local entryL_x = padding_left --左菜单x轴
	local entryL_text_x_default = entryL_x + entry_padding_left --左菜单文字x轴默认
	local entryL_text_x = entryL_text_x_default --左菜单文字x轴
	local entryR_x = entryL_x + entry_w + padding_left --右菜单x轴
	local entryR_text_x_default = entryR_x + entry_padding_left --右菜单文字x轴默认
	local entryR_text_x = entryR_text_x_default --右菜单文字x轴
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = entry_line_h*(limit/2) - entry_margin_bottom
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = entry_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end
        
        if HAVE_UPDATER then
            main_entries[12].tip = true
        else
            main_entries[12].tip = false
        end

		local listLen = #main_entries
		if main_need_refresh then
			top, bottom, main_pos = refreshListStates2(top, main_pos, limit, listLen)
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
			main_need_refresh = false
		end
		if pos_old ~= main_pos then
			pos_old = main_pos
			entryL_text_x = entryL_text_x_default
			entryR_text_x = entryR_text_x_default
		end
		--列表
		local mEntry_y = entry_y
		local mEntry_x = entryL_x
        local mEntry_text_x = entryL_text_x
        local mEntry_text_x_default = entryL_text_x_default
		local k = 1
		for i = top, bottom do
            if k > 2 then
                mEntry_y += entry_line_h
                k = 1
            end
            if k == 1 then
        		mEntry_x = entryL_x
                mEntry_text_x = entryL_text_x
                mEntry_text_x_default = entryL_text_x_default   
			elseif k == 2 then
        		mEntry_x = entryR_x
                mEntry_text_x = entryR_text_x
                mEntry_text_x_default = entryR_text_x_default
            end
            k += 1

			local mText_y = mEntry_y + entry_padding_top
			local text = main_entries[i].text or ""
			local textWidth = screen.textwidth(text, FONT_SIZE_DEFAULT)

			draw.fillrect(mEntry_x, mEntry_y, entry_w, entry_h, COLOR_TEXT_TRACK_DEFAULT)
			if i == main_pos then
				draw.fillrect(mEntry_x, mEntry_y, entry_w, entry_h, COLOR_TEXT_TRACK_POS)
			end
			if textWidth > entry_text_w then
				if i == main_pos then
					mEntry_text_x = screen.print(mEntry_text_x, mText_y, text, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __SLEFT, entry_text_w)				
				else
					screen.print(mEntry_text_x_default, mText_y, text, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ALEFT, entry_text_w)
				end
			else
				local text_x = mEntry_text_x_default + (entry_text_w - textWidth)/2
				screen.print(text_x, mText_y, text, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
			end
            if main_entries[i].tip then
                local tip_x = mEntry_x + entry_w - 30
                local tip_y = mEntry_y + entry_h/2
                draw.circle(tip_x, tip_y, 6, COLOR_RED)
            end
		end

		--滚动条
		if listLen > limit then
			draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
			draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
		end

		--顶部状态栏
		top_status_bar_draw(APP_NAME.." "..APP_VERSION_STR)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end
	
	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.select then
			ftp_server()
		end --select键

		if buttons.start then
			usb_connect()
		end --start键

		if buttons.up or buttons.analogly < -60 then --↑键
			local tmpPos = main_pos - 2
			if tmpPos >= 1 then
				main_pos = tmpPos
				main_need_refresh = true
			end
		elseif buttons.down or buttons.analogly > 60 then --↓键
			local tmpPos = main_pos + 2
			if tmpPos <= #main_entries then
				main_pos = tmpPos
				main_need_refresh = true
			end
		end
		
		if buttons.left or buttons.analoglx < -60 then --←键设置
			local tmpPos = main_pos - 1
			if tmpPos >= 1 and tmpPos%2 ~= 0 then
				main_pos = tmpPos
				--main_need_refresh = true
			end
		elseif buttons.right or buttons.analoglx > 60 then --→键设置
			local tmpPos = main_pos + 1
			if tmpPos <= #main_entries and tmpPos%2 == 0 then
				main_pos = tmpPos
				--main_need_refresh = true
			end
		end
	 
		if buttons[positive] then --确定键
			main_entries[main_pos].func()
		end
	end

	return math_func
	
end









