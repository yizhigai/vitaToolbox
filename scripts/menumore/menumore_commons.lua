
----------------重装变革固件----------------
--[[
function hekaku_reinstall()
	
	local pathToHenkakuSkprx = "resources/henkaku/henkaku.skprx"
	local pathToHenkakuSuprx = "resources/henkaku/henkaku.suprx"
	local pathToTaiSkprx = "resources/henkaku/taihen.skprx"
	local pathToUrTai = "ur0:tai/"
	copyPath(pathToHenkakuSkprx, pathToUrTai)
	copyPath(pathToHenkakuSuprx, pathToUrTai)
	copyPath(pathToTaiSkprx, pathToUrTai)
	
end
--]]

--------------重置PKGj配置文件-------------------------------
function pkgj_reset()

	local psvGameStrs = {
		PKGJ_CONFIG_GAMES_TC,
		PKGJ_CONFIG_GAMES_SC,
		PKGJ_CONFIG_GAMES_EN,
	}
	local langEntries = {
		{text = MENUMORE_PKGJ_LANGUAGE_TC, enable = true},
		{text = MENUMORE_PKGJ_LANGUAGE_SC, enable = true},
		{text = MENUMORE_PKGJ_LANGUAGE_EN, enable = true},
	}
	local langSel = ItemDialog(MENUMORE_PKGJ_LANGUAGE_SELECT, langEntries, CANCEL, POSITIVE)
	if langSel == 0 then
		return
	end
	local langText = langEntries[langSel].text
	local psvGameStr = psvGameStrs[langSel]
	
	local pspPaths = {
		"ux0:",
		"uma0:",
		"ur0:",
	}
	local pspPathEntries = {
		{text = MENUMORE_PKGJ_PATH_UX, enable = true},
		{text = MENUMORE_PKGJ_PATH_UMA, enable = true},
		{text = MENUMORE_PKGJ_PATH_UR, enable = true},
	}
	local pspPathSel = ItemDialog(MENUMORE_PKGJ_PATH_SELECT, pspPathEntries, CANCEL, POSITIVE)
	if pspPathSel == 0 then
		return
	end
	local pspPathText = pspPaths[pspPathSel]
	local pspPathStr = string.format(PKGJ_CONFIG_PSP_PSX_PATH, pspPaths[pspPathSel])
	
	local state = MessageDialog(TIPS, string.format(MENUMORE_PKGJ_RESET_READY, langText, pspPathText), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	WaitDialog(WAIT_EXECUTING)
	local completed = false

	local configStrs = {
		psvGameStr,
		PKGJ_CONFIG_DLCS,
		PKGJ_CONFIG_PSX_GAMES,
		PKGJ_CONFIG_PSP_GAMES,
		PKGJ_CONFIG_PSM_GAMES,
		pspPathStr,
		PKGJ_CONFIG_PSM_DISCLAIMER,
	}
	local partitionList = {"ux0:", "ur0:"}
	for i = 1, #partitionList do
		local configPath = partitionList[i].."/pkgj/config.txt"
		local configExist = checkFileExist(configPath)
		if not configExist and i == 1 then
			deletePath(configPath)
			if createFile(configPath) == 1 then
				configExist = true
			end
		end
		if configExist then
			if writeLines(configPath, configStrs) == 1 then
				completed = true
			end
		end
	end
	if completed then
		MessageDialog(TIPS, MENUMORE_PKGJ_RESET_COMPLETED, BACK)
	else
		MessageDialog(TIPS, MENUMORE_PKGJ_RESET_FAILED, BACK)
	end

end

----------------psvtv U盘插件----------------
function usbToolInit()

    --tf卡和usb插件
    local entries = {
        {text = INSTALL_USBMC_UX, enable = true},
        {text = INSTALL_USBMC_UX2, enable = true},
        {text = UNINSTALL_GAMESD, enable = true},
    }
  
	local sel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if sel == 1 or sel == 2 then
		local install_mode = 1
		local install_sel = 1
		if sel == 1 then
			install_sel = 3
		elseif sel == 2 then
			install_sel = 4
		end
		local state = MessageDialog(TIPS, string.format(INSTALL_USBMC_READY, entries[sel].text), CANCEL, POSITIVE)
		if state == 1 then 
			local ret = install_gamesd(install_mode, install_sel)
			if ret < 0 then
				MessageDialog(TIPS, NO_CONFIG, BACK)
			else
				local state = MessageDialog(TIPS, string.format(INSTALL_USBMC_COMPLETED, entries[sel].text), CANCEL, POSITIVE)
				if state == 1 then
					WaitDialog(WAIT_EXECUTING)
					os.delay(200)
					power.restart()
				end
			end
		end

	elseif sel == 3 then
		local state = MessageDialog(TIPS, UNINSTALL_USBMC_READY, CANCEL, POSITIVE)
		if state == 1 then
			uninstall_gamesd(3)
			local state = MessageDialog(TIPS, UNINSTALL_USBMC_COMPLETED, CANCEL, POSITIVE)
			if state == 1 then
				WaitDialog(WAIT_EXECUTING)
				os.delay(200)
				power.restart()
			end
		end
    end
end

--------------主机确认键设定-------------------------------
function button_assign_set()

	local entries = {
		{text = MENUMORE_BUTTON_ASSIGN_O, enable = true},
		{text = MENUMORE_BUTTON_ASSIGN_X, enable = true},
	}

	local assignSel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if assignSel == 0 then
		return
	end
	local state = MessageDialog(TIPS, string.format(MENUMORE_BUTTON_ASSIGN_READY, entries[assignSel].text), CANCEL, POSITIVE)
	if state == 1 then
		WaitDialog(WAIT_EXECUTING)
		if assignSel == 1 then
			buttons.assign(0)
		elseif assignSel == 2 then
			buttons.assign(1)
		end
		MessageDialog(TIPS, MENUMORE_BUTTON_ASSIGN_COMPLETED, BACK)
	end

end




