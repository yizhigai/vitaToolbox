menumore_pos = 1
menumore_need_refresh = true

function menumore_draw()

	local math_func = {}

	--选项
	local menumore_entries = {
		{text = MENUMORE_TAICFG_RELOAD, func = taicfg_reload},
		{text = MENUMORE_TAICFG_RESET, func = taicfg_reset},
		{text = MENUMORE_PKGJ_RESET, func = pkgj_reset},
        {text = MENUMORE_USBMC_TOOL, func = usbToolInit},
		{text = MENUMORE_BUTTON_ASSIGN, func = button_assign_set},
	}
		--按键说明
	local button_texts = {
		MENUMORE_BUTTON_POSITIVE,
		MENUMORE_BUTTON_BACK,
	}

	local limit = 18
	local top = 1
	local bottom = limit
	local pos_old = menumore_pos

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local list_x = padding_left
	local list_y = STATUS_BAR_HEIGHT + padding_top
	local name_x_default = list_x --名称
	local name_x = name_x_default
	local name_x2 = SCREEN_WIDTH - padding_left
	local name_w = name_x2 - name_x_default
	local list_w = name_x2 - name_x_default
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		local listLen = #menumore_entries
		if menumore_need_refresh then
			top, bottom, menumore_pos = refreshListStates(top, menumore_pos, limit, listLen)
			scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
			menumore_need_refresh = false
		end
		if pos_old ~= menumore_pos then
			pos_old = menumore_pos
			name_x = name_x_default
		end
		--列表
		local mList_y = list_y
		for i = top, bottom do
			local text_color = COLOR_TEXT_DEFAULT
			if i == menumore_pos then
				text_color = COLOR_TEXT_POS
			end
			--名称
			if  i == menumore_pos and screen.textwidth(menumore_entries[i].text or "") > name_w then
				name_x = screen.print(name_x, mList_y, menumore_entries[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, name_w)
			else
				screen.print(name_x_default, mList_y, menumore_entries[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, name_w)
			end
			mList_y += FONT_LINE_HEIGHT_DEFAULT
		end
		--滚动条
		if listLen > limit then
			draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
			draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
		end

		--顶部状态栏
		top_status_bar_draw(MENU_MORE)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.up or buttons.analogly < -60 then --↑键设
			if menumore_entries and #menumore_entries > 0 then
				if menumore_pos > 1 then
					menumore_pos -= 1
					menumore_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if menumore_entries and #menumore_entries > 0 then
				if menumore_pos < #menumore_entries then
					menumore_pos += 1
					menumore_need_refresh = true
				end
			end
		end
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end
		
		if buttons[positive] then --确定键
			menumore_entries[menumore_pos].func()
		end
	end
		
	return math_func
	
end

