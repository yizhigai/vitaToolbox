---------------初始化----------------------
if not SECOND_INTO_MUSICPLAYER then
	__MUSIC_PATH = "ux0:data/music/"
	__MUSICPLAYER_COVER = nil
	
	__MUSICPLAYER_COVER_W = 362
	__MUSICPLAYER_COVER_H = __MUSICPLAYER_COVER_W

	musicIcon = image.load("resources/icons/audio_icon.png")
	coverIcon = image.load("resources/icons/cover.png")
	image.resize(coverIcon, __MUSICPLAYER_COVER_W, __MUSICPLAYER_COVER_H)

	dofile("scripts/musicplayer/musicplayer_draw.lua")

	SECOND_INTO_MUSICPLAYER = true
end


if musicplayerFunc == nil then
	musicplayerFunc = musicplayer_draw()
end
table.insert(MATH_FUNC, musicplayerFunc)

if music_playmode == nil then
	music_playmode = 1
end

musicplayer_create_musicpath()

get_music_list(__MUSIC_PATH)



