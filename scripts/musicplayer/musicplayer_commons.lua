
----------创建音乐根目录----------
function musicplayer_create_musicpath()
	
	if checkFolderExist(__MUSIC_PATH) then
		return
	end
	if not files.exists("ux0:") then
		return
	end
	music_playlist = nil
	MUSICPLAYER_IS_LOADING = true
	WaitDialog(WAIT_EXECUTING)
	deletePath(__MUSIC_PATH)
	createFolder(__MUSIC_PATH)
	MUSICPLAYER_IS_LOADING = false
	dismissDialog()

end

-----------------获取取音乐列表---------------------
function get_music_list(path)
	
	local tmpList = files.listfiles(path)
	if not tmpList then
		music_playlist = nil
		return -1
	end
	music_playlist = {}
	table.sort(tmpList, function(a,b) return string.lower(a.name) < string.lower(b.name) end)
	local musicPlayposGeted = false
	for i = 1, #tmpList do
		local fileExt = tmpList[i].ext
		if fileExt and (fileExt == "mp3" or fileExt == "ogg" or fileExt == "wav") then
			local path = tmpList[i].path
			local name = tmpList[i].name
			tmpList[i].nameNoExt = stripExtension(name)
			table.insert(music_playlist, tmpList[i])
			if not musicplayer_posGeted and music_playpath and music_playpath == path then
				music_playpos = #music_playlist
				musicPlayposGeted = true
			end
		end
	end
	musicplayer_need_refresh = true

	return 1
	
end

-----------------获取取音乐封面---------------------
function musicplayer_get_cover()

	if not music_playpath then
		return nil
	end
	local ext = getExtension(music_playpath)
	if ext == nil or ext ~= "mp3" then
		return nil
	end
	if music_playlist[musicplayer_pos].id3 == nil then
		music_playlist[musicplayer_pos].id3 = sound.getid3(music_playlist[musicplayer_pos].path)
	end
	local cover = music_playlist[musicplayer_pos].id3.cover
	if cover == nil then
		return nil
	end
	image.resize(cover, __MUSICPLAYER_COVER_W, __MUSICPLAYER_COVER_H)

	return cover
	
end

-----------------播放---------------------
function musicplayer_play()

	if not music_playlist or #music_playlist < 1 then
		return -1
	end
	if not checkFileExist(music_playlist[musicplayer_pos].path) then
		return -1
	end
	music_playpos = musicplayer_pos
	music_playpath = music_playlist[music_playpos].path
	music_playname = music_playlist[music_playpos].nameNoExt
	if SOUND and sound.playing(SOUND) then
		sound.stop(SOUND)
	end
	SOUND = sound.load(music_playlist[music_playpos].path)
	if not SOUND then
		return -1
	end
	sound.play(SOUND)
	if music_playmode == 2 and not sound.looping(SOUND) then
		sound.loop(SOUND)
	end
	
end

-----------------暂停---------------------
function musicplayer_pause()

	if SOUND then
		if sound.endstream(SOUND) then
			sound.play(SOUND)
		else
			sound.pause(SOUND)
		end
	else
		musicplayer_play()
	end
	
end

------------------播放上一首---------------------
function music_play_previous()

	if not music_playlist or #music_playlist < 1 then
		return -1
	end
  local tmpMusicPlaypos = music_playpos
	if not tmpMusicPlaypos then
		tmpMusicPlaypos = 1
	elseif tmpMusicPlaypos > 1 then
		tmpMusicPlaypos -= 1
  end
	while not checkFileExist(music_playlist[tmpMusicPlaypos].path) and tmpMusicPlaypos > 1 do
		tmpMusicPlaypos -= 1
	end
	if not checkFileExist(music_playlist[tmpMusicPlaypos].path) then
		return -1
	end
	if music_playpos and music_playpos == tmpMusicPlaypos then
		return -1
	end
	music_playpos = tmpMusicPlaypos
	music_playpath = music_playlist[music_playpos].path
	music_playname = music_playlist[music_playpos].nameNoExt
	if SOUND and sound.playing(SOUND) then
		sound.stop(SOUND)
	end
	SOUND = sound.load(music_playlist[music_playpos].path)
	if not SOUND then
		return -1
	end
	sound.play(SOUND)
	if music_playmode == 2 and not sound.looping(SOUND) then
		sound.loop(SOUND)
	end
	
	return 1

end

------------------播放下一首---------------------
function music_play_next()

	if not music_playlist or #music_playlist < 1 then
		return -1
	end
  local tmpMusicPlaypos = music_playpos
  local listLen = #music_playlist
	if not tmpMusicPlaypos then
		tmpMusicPlaypos = 1
	elseif tmpMusicPlaypos < listLen or music_playmode == 3 then
		tmpMusicPlaypos += 1
		if tmpMusicPlaypos > listLen then
			tmpMusicPlaypos = 1
		end
  end
	while not checkFileExist(music_playlist[tmpMusicPlaypos].path) and tmpMusicPlaypos < #music_playlist do
		tmpMusicPlaypos += 1
	end
	if not checkFileExist(music_playlist[tmpMusicPlaypos].path) then
		return -1
	end
	if music_playpos and music_playpos == tmpMusicPlaypos then
		return -1
	end
	music_playpos = tmpMusicPlaypos
	music_playpath = music_playlist[music_playpos].path
	music_playname = music_playlist[music_playpos].nameNoExt
	if SOUND and sound.playing(SOUND) then
		sound.stop(SOUND)
	end
	SOUND = sound.load(music_playlist[music_playpos].path)
	if not SOUND then
		return -1
	end
	sound.play(SOUND)
	if music_playmode == 2 and not sound.looping(SOUND) then
		sound.loop(SOUND)
	end
	
	return 1
	
end

-----------------更改播放模式---------------------
function musicplayer_switch_mode()

	music_playmode += 1
	if music_playmode > 3 then
		music_playmode = 1
	end
	if music_playmode == 1 or music_playmode == 3 then
		if SOUND and sound.looping(SOUND) then
			sound.loop(SOUND)
		end		
	elseif music_playmode == 2 then
		if SOUND and not sound.looping(SOUND) then
			sound.loop(SOUND)
		end
	end
	
end
