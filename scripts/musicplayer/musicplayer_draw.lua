musicplayer_pos = 1
musicplayer_need_refresh = true

function musicplayer_draw()

	local math_func = {}

	--按键说明
	local button_texts = {
		MUSICPLAYER_BUTTON_PLAY,
		MUSICPLAYER_BUTTON_SWITCH_MODE,
		MUSICPLAYER_BUTTON_POSITIVE,
		MUSICPLAYER_BUTTON_BACK,
		MUSICPLAYER_BUTTON_LR,
		MUSICPLAYER_BUTTON_START,
	}
	--播放模式
	local playmodes = {
		MUSICPLAYER_BUTTON_ORDER_PLAY,
		MUSICPLAYER_BUTTON_SINGLE_REPEAT,
		MUSICPLAYER_BUTTON_CYCLE_PLAY,
	}

	local limit = 17
	local top = 1
	local bottom = limit
	local pos_old = musicplayer_pos

	--全局
	local padding_top = 12
	local padding_left = 30
	--当前播放音乐名
	local playname_x_default = padding_left
	local playname_x = playname_x_default
	local playname_y = STATUS_BAR_HEIGHT + padding_top
	local playname_w = SCREEN_WIDTH_HALF - padding_left*2
	--音乐封面
	local cover_margin_top = padding_top
	local cover_x = (SCREEN_WIDTH_HALF - __MUSICPLAYER_COVER_W)/2
	local cover_y = playname_y + FONT_HEIGHT_DEFAULT + cover_margin_top
	--播放时间
	local playtime_margin_top = cover_margin_top
	local playtime_x = padding_left
	local playtime_y = cover_y + __MUSICPLAYER_COVER_H + playtime_margin_top
	--播放模式
	local music_playmode_margin_top = playtime_margin_top
	local music_playmode_x = SCREEN_WIDTH_HALF - padding_left
	local music_playmode_y = playtime_y
	--播放进度条
	local progressbar_track_margin_top = 8
	local progressbar_track_w = SCREEN_WIDTH_HALF - padding_left*2
	local progressbar_track_h = 6
	local progressbar_track_x = playtime_x
	local progressbar_track_y = playtime_y + FONT_HEIGHT_DEFAULT + progressbar_track_margin_top
	local progressbar_h = progressbar_track_h
	local progressbar_x = progressbar_track_x
	local progressbar_y = progressbar_track_y
	--列表标题和数量
	local list_title_margin_top = padding_top
	local list_title_x = SCREEN_WIDTH/4*3
	local list_title_y = playname_y
	local list_title_w = playname_w
	--列表
	local list_x = SCREEN_WIDTH_HALF
	local list_y = cover_y
	--文件图标
	local list_fileicon_x = list_x
	local list_name_x_default = list_fileicon_x + 24
	local list_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	--列表文件名称
	local list_name_x_default = 504
	local list_name_x = list_name_x_default
	local list_name_w = 426
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = list_h
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		--当前播放音乐名
		if music_playname then
			local mPlayname_w = screen.textwidth(music_playname)
			if mPlayname_w > playname_w then
				playname_x = screen.print(playname_x, playname_y, music_playname, FONT_SIZE_DEFAULT, COLOR_GREEN, COLOR_BLACK, __SLEFT, playname_w)
			else
				local mPlayname_x = playname_x_default + (playname_w - mPlayname_w)/2
				screen.print(mPlayname_x, playname_y, music_playname, FONT_SIZE_DEFAULT, COLOR_GREEN, COLOR_BLACK)
			end
		end
		--封面
		if __MUSICPLAYER_COVER then
			image.blit(__MUSICPLAYER_COVER, cover_x, cover_y)
		else
			image.blit(coverIcon, cover_x, cover_y)
		end
		--播放进度条背景
		draw.fillrect(progressbar_track_x, progressbar_track_y, progressbar_track_w, progressbar_track_h, COLOR_PROGRESS_BAR_TRACK) 
		--播放时间
		local playtime = "00:00:00"
		button_texts[1] = MUSICPLAYER_BUTTON_PLAY
		if SOUND then
			playtime = tostring(sound.time(SOUND))
			--播放进度条
			local playpcrcent = sound.percent(SOUND)
			--滚动条
			progressbar_w = (progressbar_track_w/100)*playpcrcent
			draw.fillrect(progressbar_x, progressbar_y, progressbar_w, progressbar_h, COLOR_PROGRESS_BAR)     
			--修改按键说明
			if sound.playing(SOUND) then
				button_texts[1] = MUSICPLAYER_BUTTON_PARSE
			end
		end
		--播放时间
		screen.print(playtime_x, playtime_y, playtime, FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
		--播放模式
		screen.print(music_playmode_x, music_playmode_y, playmodes[music_playmode], FONT_SIZE_DEFAULT, COLOR_GREEN, COLOR_BLACK, __ARIGHT)
		--列表
		if music_playlist and #music_playlist > 0 then
			local listLen = #music_playlist
			if musicplayer_need_refresh then
				top, bottom, musicplayer_pos = refreshListStates(top, musicplayer_pos, limit, listLen)
				scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
				musicplayer_need_refresh = false
			end
			if pos_old ~= musicplayer_pos then
				pos_old = musicplayer_pos
				list_name_x = list_name_x_default
			end
		 --列表标题(数量)
		 screen.print(list_title_x, list_title_y, MUSICPLAYER_PLAYLIST.." ("..#music_playlist..")", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ACENTER, list_title_w)
			--列表
			local mList_y = list_y
			for i = top, bottom do
				local text_color = COLOR_TEXT_DEFAULT
				if i == musicplayer_pos then
					text_color = COLOR_TEXT_POS
				elseif music_playpath and music_playlist[i].path == music_playpath then
					text_color = COLOR_GREEN
				end
				--文件图标
				if musicIcon then
					image.blit(musicIcon, list_fileicon_x, mList_y)
				end
				--文件名
				local name = music_playlist[i].nameNoExt
				if  i == musicplayer_pos and screen.textwidth(name or "") > list_name_w then
					list_name_x = screen.print(list_name_x, mList_y, name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, list_name_w)
				else
					screen.print(list_name_x_default, mList_y, name or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, list_name_w)
				end
				mList_y += FONT_LINE_HEIGHT_DEFAULT
			end
			--滚动条
			if listLen > limit then
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
			end
		elseif not MUSICPLAYER_IS_LOADING then
			screen.print(list_x, list_y, MUSICPLAYER_SONG_NOFOUND, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK)
		end
	
		--顶部状态栏
		top_status_bar_draw(MUSIC_PLAYER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.start then --start键
			power.display(0)
		end

		if buttons.up or buttons.analogly < -60 then --↑键设
			if music_playlist and #music_playlist > 0 then
				if musicplayer_pos > 1 then
					musicplayer_pos -= 1
					musicplayer_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if music_playlist and #music_playlist > 0 then
				if musicplayer_pos < #music_playlist then
					musicplayer_pos += 1
					musicplayer_need_refresh = true
				end
			end
		end

		if buttons.l then
			if music_play_previous() == 1 then
				musicplayer_need_refresh = true
			end
		end --L键
		
		if buttons.r then
			if music_play_next() == 1 then
				musicplayer_need_refresh = true
			end
		end --R键
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end

		if buttons.square then --□键
			musicplayer_pause()
		end
	
		if buttons.triangle then --△键
			musicplayer_switch_mode()
		end
		
		if buttons[positive] then --确定键
			musicplayer_play()
		end
	end
		
	return math_func
	
end

