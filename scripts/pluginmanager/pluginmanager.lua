---------------初始化---------------
if not SECOND_INTO_PLUGINMANAGER then
	pluginsDataRootPath = "resources/vita_plugins"

	dofile("scripts/pluginmanager/pluginmanager_list.lua")
	dofile("scripts/pluginmanager/pluginmanager_commons.lua")
	dofile("scripts/pluginmanager/pluginmanager_draw.lua")

	pluginmanager_get_list()

	SECOND_INTO_PLUGINMANAGER = true
end

if pluginmanagerFunc == nil then
	pluginmanagerFunc = pluginmanager_draw()
end
table.insert(MATH_FUNC, pluginmanagerFunc)

unmarkAll(pluginmanager_list)


