-------------------获取列表--------------------------
function pluginmanager_get_list()

	local k = 1
	for i = 1, #pluginmanager_list do
		if (pluginmanager_list[k].min_version and VITA_SWVERSION < pluginmanager_list[k].min_version) or (pluginmanager_list[k].max_version and VITA_SWVERSION > pluginmanager_list[k].max_version) then
			table.remove(pluginmanager_list, k)
		else
			k += 1
		end
	end
	pluginmanager_need_refresh = true	

end

-------------------安裝插件修改config--------------------------
function install_plugin_modifyConfig(configPath, installInfo)

	local input = io.open(configPath, "r")
	if input == nil then
		return -1
	end

	local mode = installInfo.mode
	local name = installInfo.name
	local check = installInfo.check
	local section = installInfo.section
	local value = installInfo.value
	local nameLow = string.lower(name)

	local contenido = {}
	local getSection = false
	local getInsertNum = false
	local insertNum = -1

	for linea in input:lines() do
		while true do
			local mLinea = trimString(linea)
			if mode == 2 or mode == 3 then
				local tmpLinea = mLinea:match("(.+)%s+%d?$")
				if tmpLinea then
					mLinea = trimString(tmpLinea)
				end
			end
			local lineName = getFileName(mLinea)
			local lineNameLow = string.lower(lineName)
			if check and lineNameLow == nameLow then
				break
			end
			table.insert(contenido, linea)
			if mode ~= 1 then
				break
			end
			if not getSection then
				if mLinea == section then
					insertNum = #contenido + 1
					getSection = true
				end
				break
			end
			if not getInsertNum then
				local firstChar = string.sub(mLinea, 1, 1)
				if firstChar and firstChar == "*" then
					insertNum = #contenido
					getInsertNum = true
					break
				end
				insertNum = #contenido + 1        
			end
			break
		end
	end
	input:close()

	if mode == 1 then
		if getSection then
			table.insert(contenido, insertNum, value)
		else
			table.insert(contenido, section)
			table.insert(contenido, value)
		end
	elseif mode == 2 or mode == 3 then
		table.insert(contenido, value)
	end

	return writeLines(configPath, contenido)
 
end

-------------------安裝插件--------------------------
function install_plugin(installInfos, partitionList, configHadBackups)
 
	local completed_counts = 0
	local installCompleted = true
	
	for i = 1, #installInfos.srcpaths do

		local mode = installInfos.mode
		local srcpath = pluginsDataRootPath..installInfos.srcpaths[i]
		local name = getFileName(srcpath)
		local dstdir = installInfos.dstdirs[i]
		local config = installInfos.configs[i]
		local check = installInfos.checks[i]
		local section = installInfos.sections[i]
		local value = dstdir..name
		if mode == 2 then
			value = value.." 1"
		elseif mode == 3 then
			value = "ms0:"..value.." 1"
		end
		local installInfo = {mode = mode, name = name, check = check, section = section, value = value}

		local prxHadCopyed = false
		for k = 1, #partitionList do
			local completed = true
			local partitionPath = partitionList[k]
			while true do
				if (mode == 2 and partitionPath ~= "ux0:") then
					break
				end
				if not config then
					break
				end
				local configPath = partitionPath..config
				local configExist = checkFileExist(configPath)
				if not configExist then
					if (mode == 1 and partitionPath == "ur0:") or (mode == 2 and partitionPath == "ux0:") or mode == 3 then
						deletePath(configPath)
						if not createFile(configPath) then
							completed = false
							break
						end
					else
						completed = false
						break
					end
				else
					if not configHadBackups[configPath] then --备份config.txt
						copyFile(configPath, configPath..".bak")
					end
					configHadBackups[configPath] = true
				end
				if install_plugin_modifyConfig(configPath, installInfo) ~= 1 then
					completed = false
					break
				end	
				break
			end
			--复制插件
			if mode == 3 and dstdir then
				local mDstdir = partitionPath.."/pspemu"..dstdir
				if files.copy(srcpath, mDstdir) ~= 1 then
					completed = false
				end
			end
			if completed then
				completed_counts += 1
			end
		end
		if (mode == 1 or mode == 2) and dstdir and name ~= "adrenaline_kernel.skprx" then
			if files.copy(srcpath, dstdir) ~= 1 then
				installCompleted = false
			end
		end
	end

	if completed_counts < 1 or not installCompleted then
		return -1
	end

	return 1

end

-------------------安裝插件复数--------------------------
function install_plugins(mode)
 
	WaitDialog(WAIT_EXECUTING)

	local installList = {}
	local posIsMarked = pluginmanager_list[pluginmanager_pos].mark
	if mode == 2 or not posIsMarked then
		table.insert(installList, pluginmanager_list[pluginmanager_pos])
	else
		for i = 1, #pluginmanager_list do
			if pluginmanager_list[i].mark then
				table.insert(installList, pluginmanager_list[i])
			end
		end
	end
	local install_counts = #installList
	local state = MessageDialog(TIPS, string.format(PLUGINMANAGER_INSTALL_PLUGIN_READY, install_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	lock_home_key()
	WaitDialog(WAIT_EXECUTING)

	local partitionList = {"ux0:", "ur0:", "uma0:", "imc0:", "xmc0:"}
	local configHadBackups = {false, false, false, false, false}
	local install_counts = #installList
	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	for i = 1, install_counts do
		if ProgressDialog(NOW_INSTALLING, installList[i].text, percent, CANCEL) == 0 then
			break
		end
		local ret = install_plugin(installList[i], partitionList, configHadBackups)
		if ret == 1 then
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/install_counts)*100)
		if ProgressDialog(NOW_INSTALLING, installList[i].text, percent, CANCEL) == 0 then
			break
		end		
	end
	unmarkAll(pluginmanager_list)

	unlock_home_key()
	dismissDialog()

	local state = MessageDialog(TIPS, string.format(PLUGINMANAGER_INSTALL_PLUGIN_COMPLETED, completed_counts, failed_counts), BACK, POWER_RESTART, RELOAD_TAICFG)
	if state == 1 then
		WaitDialog(WAIT_EXECUTING)
		os.delay(200)
		power.restart()
	elseif state == 2 then
		taicfg_reload()
	end

end

-------------------卸载插件修改文本--------------------------
function uninstall_plugin_modifyConfig(configPath, uninstallInfo)

	local input = io.open(configPath, "rb")
	if input == nil then
		return 1
	end

	local mode = uninstallInfo.mode
	local name = uninstallInfo.name
	local section = uninstallInfo.section
	local nameLow = string.lower(name)

	local contenido = {}
	local getSection = false
	local stopCheckDeleteSection = false
	local sectionNum = -1

	for linea in input:lines() do
		while true do
			local mLinea = trimString(linea)
			if mode == 2 or mode == 3 then
				mLinea = mLinea:match("(.+)%s+%d?$") or ""
			end
			local lineName = getFileName(mLinea)
			local lineNameLow = string.lower(lineName)
			if lineNameLow == nameLow then
				break
			end
			table.insert(contenido, linea)
			if mode ~= 1 or (getSection and stopCheckDeleteSection) then
				break
			end
			if not getSection then
				if mLinea == section then
					sectionNum = #contenido
					getSection = true
				end
				break
			end
			if not stopCheckDeleteSection then
				local firstChar = string.sub(mLinea, 1, 1)
				if not firstChar then
					break
				end
				if firstChar == "*" then
					table.remove(contenido, sectionNum)
				end
				stopCheckDeleteSection = true
			end
			break
		end
	end
	input:close()

	if getSection and not stopCheckDeleteSection then
		table.remove(contenido, sectionNum)
	end

	return  writeLines(configPath, contenido)
 
end

-------------------卸载插件--------------------------
function uninstall_plugin(uninstallInfos, partitionList, configHadBackups)
 
	for i = 1, #uninstallInfos.srcpaths do
		while true do 
			if uninstallInfos.commons and uninstallInfos.commons[i] then
				break
			end
			local mode = uninstallInfos.mode
			local name = getFileName(uninstallInfos.srcpaths[i])
			local dstdir = uninstallInfos.dstdirs[i]
			local config = uninstallInfos.configs[i]
			local section = uninstallInfos.sections[i]
			local delete = uninstallInfos.deletes[i]

			local uninstallInfo = {mode = mode, name = name, section = section}

			local prxHadDeleted = false
			for k = 1, #partitionList do
				local partitionPath = partitionList[k]
				--修改文本
				if config and not (mode == 2 and partitionPath ~= "ux0:") then
					local configPath = partitionPath..config
					local configExist = checkFileExist(configPath)
					if configExist then
						if not configHadBackups[configPath] then --备份config.txt
							copyFile(configPath, configPath..".bak")
							configHadBackups[configPath] = true
						end
						uninstall_plugin_modifyConfig(configPath, uninstallInfo)
					end
				end
				--删除插件
				if delete and dstdir then
					local dstPath = dstdir..name
					if mode == 3 then
						dstPath = partitionPath.."/pspemu"..dstPath
						deletePath(dstPath)
					elseif not prxHadDeleted then
						deletePath(dstPath)
						prxHadDeleted = true
					end
				end
			end
			break
		end
	end

end

-------------------卸载插件复数--------------------------
function uninstall_plugins(mode)

	WaitDialog(WAIT_EXECUTING)

	local uninstallList = {}
	local posIsMarked = pluginmanager_list[pluginmanager_pos].mark
	if mode == 2 or not posIsMarked then
		table.insert(uninstallList, pluginmanager_list[pluginmanager_pos])
	else
		for i = 1, #pluginmanager_list do
			if pluginmanager_list[i].mark then
				table.insert(uninstallList, pluginmanager_list[i])
			end
		end
	end
	local install_counts = #uninstallList
	local state = MessageDialog(TIPS, string.format(PLUGINMANAGER_UNINSTALL_PLUGIN_READY, install_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	lock_home_key()
	WaitDialog(WAIT_EXECUTING)

	local partitionList = {"ux0:", "ur0:", "uma0:", "imc0:", "xmc0:"}
	local configHadBackups = {false, false, false, false, false}
	local uninstall_counts = #uninstallList
	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	for i = 1, uninstall_counts do
		if ProgressDialog(NOW_UNINSTALLING, uninstallList[i].text, percent, CANCEL) == 0 then
			break
		end
		uninstall_plugin(uninstallList[i], partitionList, configHadBackups)
		completed_counts += 1
		percent = math.floor((i/uninstall_counts)*100)
		if ProgressDialog(NOW_UNINSTALLING, uninstallList[i].text, percent, CANCEL) == 0 then
			break
		end		
	end
	unmarkAll(pluginmanager_list)

	unlock_home_key()
	dismissDialog()

	local state = MessageDialog(TIPS, string.format(PLUGINMANAGER_UNINSTALL_PLUGIN_COMPLETED, completed_counts), CANCEL, POSITIVE)
	if state == 1 then
		WaitDialog(WAIT_EXECUTING)
		os.delay(200)
		power.restart()
	end

end

-------------------查看说明--------------------------
function pluginmanager_open_about()

	local select_counts = 1
	local aboutText = pluginmanager_list[pluginmanager_pos].about
	local state = MessageDialog(INTRODUCTION, aboutText, BACK, INSTALL, UNINSTALL)
	if state == 1 then
		install_plugins(2)
	elseif state == 2 then
		uninstall_plugins(2)
	end
 
end

-------------------打开菜单--------------------------
function pluginmanager_open_menu()

	local entries = {
		{text = PLUGINMANAGER_INSTALL_PLUGIN, enable = true},
		{text = PLUGINMANAGER_UNINSTALL_PLUGIN, enable = true},
		{text = PLUGINMANAGER_TAICFG_RELOAD, enable = true},
		{text = PLUGINMANAGER_TAICFG_RESET, enable = true},
		{text = PLUGINMANAGER_PLUGIN_ABOUT, enable = true},
	}

	local sel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if sel == 1 then --安装
		install_plugins(1)

	elseif sel == 2 then --卸载
		uninstall_plugins(1)

	elseif sel == 3 then --重载taiHen
		taicfg_reload()

	elseif sel == 4 then --重置taiHen
		taicfg_reset()

	elseif sel == 5 then --关于
		MessageDialog(PLUGINMANAGER_PLUGIN_ABOUT, PLUGINMANAGER_ABOUT, BACK)

	end

end

