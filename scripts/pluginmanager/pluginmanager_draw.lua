pluginmanager_pos = 1
pluginmanager_need_refresh = true

function pluginmanager_draw()

	local math_func = {}

	--按键说明
	local button_texts = {
		PLUGINMANAGER_BUTTON_MARK,
		PLUGINMANAGER_BUTTON_OPEN_MENU,
		PLUGINMANAGER_BUTTON_POSITIVE,
		PLUGINMANAGER_BUTTON_BACK,
	}

	local limit = 18
	local top = 1
	local bottom = limit
	local pos_old = pluginmanager_pos

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local list_x = padding_left
	local list_y = STATUS_BAR_HEIGHT + padding_top
	local mode_w = 8 --当前账号标记
	local mode_h = mode_w
	local mode_margin_top = (FONT_HEIGHT_DEFAULT - mode_h)/2
	local mode_margin_right = 5
	local mode_x = list_x - mode_margin_right - mode_w
	local ordinal_x = list_x --序数
	local name_x_default = 78 --名称
	local name_x = name_x_default
	local name_x2 = SCREEN_WIDTH - padding_left
	local name_w = name_x2 - name_x_default
	local list_w = name_x2 - ordinal_x
	local mark_padding_top = 3 --标记
	local mark_x = list_x
	local mark_w = list_w
	local mark_h = FONT_HEIGHT_DEFAULT + mark_padding_top*2
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		if pluginmanager_list and #pluginmanager_list > 0 then
			local listLen = #pluginmanager_list
			if pluginmanager_need_refresh then
				top, bottom, pluginmanager_pos = refreshListStates(top, pluginmanager_pos, limit, listLen)
				scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
				pluginmanager_need_refresh = false
			end
			if pos_old ~= pluginmanager_pos then
				pos_old = pluginmanager_pos
				name_x = name_x_default
			end
			--列表
			local mList_y = list_y
			for i = top, bottom do
				--插件模式提示
				local mode_color = COLOR_GREEN
				if pluginmanager_list[i].tai_mode == 1 then
					mode_color = COLOR_RED
				elseif pluginmanager_list[i].tai_mode == 2 then
					mode_color = COLOR_YELLOW
				end
				draw.fillrect(mode_x, mList_y + mode_margin_top, mode_w, mode_h, mode_color)
				--标记
				if pluginmanager_list[i].mark then
					local mark_y = mList_y - mark_padding_top
					draw.fillrect(mark_x, mark_y, mark_w, mark_h, COLOR_MARK) 
				end
				
				local text_color = COLOR_TEXT_DEFAULT
				if i == pluginmanager_pos then
					text_color = COLOR_TEXT_POS
				end
				--序数
				screen.print(ordinal_x, mList_y, string.format("%03d", i), FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				--名称
				if  i == pluginmanager_pos and screen.textwidth(pluginmanager_list[i].text or "") > name_w then
					name_x = screen.print(name_x, mList_y, pluginmanager_list[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, name_w)
				else
					screen.print(name_x_default, mList_y, pluginmanager_list[i].text or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, name_w)
				end
				mList_y += FONT_LINE_HEIGHT_DEFAULT
			end
			--滚动条
			if listLen > limit then
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
			end
		end

		--顶部状态栏
		top_status_bar_draw(PLUGIN_MANAGER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.up or buttons.analogly < -60 then --↑键设
			if pluginmanager_list and #pluginmanager_list > 0 then
				if pluginmanager_pos > 1 then
					pluginmanager_pos -= 1
					pluginmanager_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if pluginmanager_list and #pluginmanager_list > 0 then
				if pluginmanager_pos < #pluginmanager_list then
					pluginmanager_pos += 1
					pluginmanager_need_refresh = true
				end
			end
		end
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end

		if buttons.square then --□键
			if pluginmanager_list and #pluginmanager_list > 0 then
				if pluginmanager_list[pluginmanager_pos].mark then
					pluginmanager_list[pluginmanager_pos].mark = false
				else
					pluginmanager_list[pluginmanager_pos].mark = true
				end
			end
		end
	
		if buttons.triangle then --△键
			pluginmanager_open_menu()
		end
		
		if buttons[positive] then --确定键
			pluginmanager_open_about()
		end
	end
		
	return math_func
	
end

