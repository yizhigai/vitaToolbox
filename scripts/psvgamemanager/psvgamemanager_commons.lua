
-------------获取游戏列表-------------
function psvgamemanager_get_list()
 
	PSVMANAGER_IS_LOADING = true
	psvgamemanager_list = nil
	ProgressDialog(NOW_LOADING, "", 0, nil)

	local tmpList = game.list(3)
	if tmpList and #tmpList > 0 then
		table.sort(tmpList, function (a,b) return string.lower(a.id) < string.lower(b.id); end)
		local game_counts = #tmpList
		local percent = 0
		local iconPath = ""
		for i = 1, game_counts do
			tmpList[i].title = string.gsub(tmpList[i].title, "\n", " ")
			iconPath = "ur0:/appmeta/"..tmpList[i].id.."/icon0.png"
			tmpList[i].icon = image.load(iconPath)
			percent = math.floor((i/game_counts)*100)
			ProgressDialog(NOW_LOADING, tmpList[i].title, percent)	
		end
	else
		ProgressDialog(NOW_LOADING, "", 100, nil)
	end
	PSVMANAGER_IS_LOADING = false
	psvgamemanager_list = tmpList
	psvgamemanager_pos = 1
	psvgamemanager_need_refresh = true
	dismissDialog()
 
end

--------------刷新游戏------------------
function psvgamemanager_refresh_game()

	WaitDialog(WAIT_EXECUTING)
	
	local refreshList = {}
	local posIsMarked = psvgamemanager_list[psvgamemanager_pos].mark
	if not posIsMarked then
		table.insert(refreshList, psvgamemanager_list[psvgamemanager_pos])
		refreshList[#refreshList].pos = psvgamemanager_pos
	else
		for i = 1, #psvgamemanager_list do
			if psvgamemanager_list[i].mark then
				table.insert(refreshList, psvgamemanager_list[i])
				refreshList[#refreshList].pos = i
			end
		end
	end
	local refresh_counts = #refreshList
	local state = MessageDialog(TIPS, string.format(PSVGAMEMANAGER_REFRESH_GAME_READY, refresh_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	for i = 1, refresh_counts do
		if ProgressDialog(NOW_REFRESHLLING, refreshList[i].title, percent, CANCEL) == 0 then
			break
		end
		local ret = install_game_dir(refreshList[i].path)
		if ret == 1 then
			psvgamemanager_list[refreshList[i].pos].title = string.gsub(psvgamemanager_list[refreshList[i].pos].title, "\n", " ")
			local iconPath = "ur0:/appmeta/"..refreshList[i].id.."/icon0.png"
			psvgamemanager_list[refreshList[i].pos].icon = image.load(iconPath)
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/refresh_counts)*100)
		if ProgressDialog(NOW_REFRESHLLING, refreshList[i].title, percent, CANCEL) == 0 then
			break
		end		
	end
	unmarkAll(psvgamemanager_list)
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(PSVGAMEMANAGER_REFRESH_GAME_COMPLETED, completed_counts, failed_counts), BACK)

end

------------------修改游戏名称---------------------
function psvgamemanager_rename_game()
 
	WaitDialog(WAIT_EXECUTING)
	
	local filePath = psvgamemanager_list[psvgamemanager_pos].path
	if checkFolderExist(filePath.."/sce_pfs") then
		MessageDialog(TIPS, PSVGAMEMANAGER_RENAME_GAME_DISABLE, BACK)
		return
	end
	local devPath = getRootPath(filePath)
	if devPath ~= "ux0:" then
		MessageDialog(TIPS, PSVGAMEMANAGER_RENAME_GAME_NOT_UX, BACK)
		return 
	end
	local sfoPath = filePath.."/sce_sys/param.sfo"
	if not checkFileExist(sfoPath) then
		MessageDialog(TIPS, PSVGAMEMANAGER_RENAME_GAME_NO_SFO, BACK)
		return  
	end
	local oldName = psvgamemanager_list[psvgamemanager_pos].title
	local newName = osk.init(PSVGAMEMANAGER_RENAME_GAME, oldName)
	if not newName or newName == "" or newName == oldName then
		dismissDialog()
		return
	end
	local state = MessageDialog(TIPS, string.format(PSVGAMEMANAGER_RENAME_GAME_READY, newName), CANCEL, POSITIVE)
	if state == 0 then
		return
	end

	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local ret = -1
	--备份sfo文件
	local backupPath = devPath.."/temp/rename_app_tmp"
	local sfoBackupPath = backupPath.."/param.sfo"
	deletePath(sfoBackupPath)
	copyPath(sfoPath, backupPath)
	--修改sfo文件
	local stitleRet = game.setsfo(sfoPath, "STITLE", newName)
	local titleRet = game.setsfo(sfoPath, "TITLE", newName)
	if stitleRet == 1 and titleRet == 1 then
		game.setsfo(sfoPath, "STITLE_"..langs[os.language()], newName)
		game.setsfo(sfoPath, "TITLE_"..langs[os.language()], newName)
		ret = install_game_dir(filePath)
	end
	--安装结果反馈
	local retStr = ""
	if ret == 1 then
		deletePath(backupPath)
		unmarkAll(psvgamemanager_list)
		psvgamemanager_list[psvgamemanager_pos].mark = true
		psvgamemanager_list[psvgamemanager_pos].title = newName
		retStr = PSVGAMEMANAGER_RENAME_GAME_COMPLETED
	else
		movePath(sfoBackupPath, getParentPath(sfoPath))
		deletePath(backupPath)
		retStr = PSVGAMEMANAGER_RENAME_GAME_FAILED
	end
	
	unlock_home_key()
	MessageDialog(TIPS, retStr, BACK)
 
end

---------------删除存档------------------------
function psvgamemanager_delete_savedata()

	WaitDialog(WAIT_EXECUTING)
	
	local deleteList = {}
	local posIsMarked = psvgamemanager_list[psvgamemanager_pos].mark
	if not posIsMarked then
		table.insert(deleteList, psvgamemanager_list[psvgamemanager_pos])
	else
		for i = 1, #psvgamemanager_list do
			if psvgamemanager_list[i].mark then
				table.insert(deleteList, psvgamemanager_list[i])
			end
		end
	end
	local delete_counts = #deleteList
	local state = MessageDialog(TIPS, string.format(PSVGAMEMANAGER_DELETE_SAVEDATA_READY, delete_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	WaitDialog(WAIT_EXECUTING)
	lock_home_key()
	
	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	for i = 1, delete_counts do
		if ProgressDialog(NOW_DELETING, deleteList[i].title, percent, CANCEL) == 0 then
			break
		end
		local savedataPath = "ux0:/user/00/savedata/"..deleteList[i].id
		local ret = deletePath(savedataPath)
		if ret == 1 then
			completed_counts += 1
		else
			failed_counts += 1
		end
		percent = math.floor((i/delete_counts)*100)
		if ProgressDialog(NOW_DELETING, deleteList[i].title, percent, CANCEL) == 0 then
			break
		end		
	end
	unmarkAll(psvgamemanager_list)
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(PSVGAMEMANAGER_DELETE_SAVEDATA_COMPLETED, completed_counts, failed_counts), BACK)

end

---------------删除游戏------------------------
function psvgamemanager_delete_game()

	WaitDialog(WAIT_EXECUTING)
	
	local deleteList = {}
	local posIsMarked = psvgamemanager_list[psvgamemanager_pos].mark
	if not posIsMarked then
		table.insert(deleteList, psvgamemanager_list[psvgamemanager_pos])
		deleteList[#deleteList].pos = psvgamemanager_pos
	else
		for i = 1, #psvgamemanager_list do
			if psvgamemanager_list[i].mark then
				table.insert(deleteList, psvgamemanager_list[i])
				deleteList[#deleteList].pos = i
			end
		end
	end
	local delete_counts = #deleteList
	local state = MessageDialog(TIPS, string.format(PSVGAMEMANAGER_DELETE_GAME_READY, delete_counts), CANCEL, POSITIVE)
	if state == 0 then
		return
	end
	
	WaitDialog(WAIT_EXECUTING)
	lock_home_key()

	local completed_counts = 0
	local failed_counts = 0
	local percent = 0
	local oldPos = psvgamemanager_pos
	for i = 1, delete_counts do
		if ProgressDialog(NOW_DELETING, deleteList[i].title, percent, CANCEL) == 0 then
			break
		end
		local ret = game.delete(deleteList[i].id)
		if ret == 1 then
			table.remove(psvgamemanager_list, deleteList[i].pos - completed_counts)
			if oldPos > deleteList[i].pos then
				psvgamemanager_pos -= 1
			end
			completed_counts += 1
			psvgamemanager_need_refresh = true
		else
			failed_counts += 1
		end
		percent = math.floor((i/delete_counts)*100)
		if ProgressDialog(NOW_DELETING, deleteList[i].title, percent, CANCEL) == 0 then
			break
		end		
	end
	unmarkAll(psvgamemanager_list)
	
	unlock_home_key()
	dismissDialog()
	
	MessageDialog(TIPS, string.format(PSVGAMEMANAGER_DELETE_GAME_COMPLETED, completed_counts, failed_counts), BACK)

end

---------------打开菜单------------------------
function psvgamemanager_open_menu()

	local entries = {
		{text = PSVGAMEMANAGER_REFRESH_GAME, enable = true},
		{text = PSVGAMEMANAGER_RENAME_GAME, enable = true},
		{text = PSVGAMEMANAGER_DELETE_SAVEDATA, enable = true},
		{text = PSVGAMEMANAGER_DELETE_GAME, enable = true},
	}
	if not psvgamemanager_list or #psvgamemanager_list < 1 then
		entries[1].enable = false
		entries[2].enable = false
		entries[3].enable = false
		entries[4].enable = false
	end

	local sel = ItemDialog(PLEASE_SELECT, entries, CANCEL, POSITIVE)
	if sel == 1 then --刷新选中的游戏
		psvgamemanager_refresh_game()
		
	elseif sel == 2 then --修改游戏名称
		psvgamemanager_rename_game()
		
	elseif sel == 3 then --删除存档
		psvgamemanager_delete_savedata()
		
	elseif sel == 4 then --删除游戏
		psvgamemanager_delete_game()
	end

end

