psvgamemanager_pos = 1
psvgamemanager_need_refresh = true

function psvgamemanager_draw()

	local math_func = {}

	--按键说明
	local button_texts = {
		PSVGAMEMANAGER_BUTTON_MARK,
		PSVGAMEMANAGER_BUTTON_OPEN_MENU,
		PSVGAMEMANAGER_BUTTON_LAUNCH_GAME,
		PSVGAMEMANAGER_BUTTON_BACK,
		PSVGAMEMANAGER_BUTTON_REFRESH_GAME_LIST,
	}

	local limit = 18
	local top = 1
	local bottom = limit
	local pos_old = psvgamemanager_pos

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local list_x = padding_left
	local list_y = STATUS_BAR_HEIGHT + padding_top
	local amount_y = list_y --总数
	local icon_margin_top = 22 --图标
	local icon_margin_right = 48
	local icon_w = 128
	local icon_h = 128
	local icon_x = SCREEN_WIDTH - (icon_w + icon_margin_right)
	local icon_y = amount_y + FONT_HEIGHT_DEFAULT + icon_margin_top
	local amount_x = icon_x + icon_w/2
	local version_margin_top = icon_margin_top --版本
	local version_margin_left = 18
	local version_y = icon_y + icon_h + version_margin_top
	local versionnum_x = SCREEN_WIDTH - icon_margin_right + 16
	local versiontitle_x = icon_x - 16
	local ordinal_x = list_x --序数
	local id_x = 78 --ID
	local title_x_default = 234 --名称
	local title_x = title_x_default
	local title_x2 = versiontitle_x - version_margin_left
	local title_w = title_x2 - title_x_default
	local list_w = title_x2 - ordinal_x
	local list_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local mark_padding_top = 3 --标记
	local mark_x = list_x
	local mark_w = list_w
	local mark_h = FONT_HEIGHT_DEFAULT + mark_padding_top*2
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = list_h
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if BACKGROUND then BACKGROUND:blit(0, 0) end

		if psvgamemanager_list and #psvgamemanager_list > 0 then
			local listLen = #psvgamemanager_list
			if psvgamemanager_need_refresh then
				top, bottom, psvgamemanager_pos = refreshListStates(top, psvgamemanager_pos, limit, listLen)
				scrollbar_h, scrollbar_y = refreshScrollbarStates(top, limit, listLen, scrollbar_track_h, scrollbar_track_y)
				psvgamemanager_need_refresh = false
			end
			if pos_old ~= psvgamemanager_pos then
				pos_old = psvgamemanager_pos
				title_x = title_x_default
			end
			--总数
			screen.print(amount_x, amount_y, string.format(PSVGAMEMANAGER_GAME_AMOUNT, #psvgamemanager_list), FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK, __ACENTER)
			--图标
			local icon = psvgamemanager_list[psvgamemanager_pos].icon
			if icon then image.blit(icon, icon_x, icon_y) end
			--版本
			local version = psvgamemanager_list[psvgamemanager_pos].version
			screen.print(versiontitle_x, version_y, PSVGAMEMANAGER_VERSION_TITLE, FONT_SIZE_DEFAULT, COLOR_TEXT_POS, COLOR_BLACK)
			screen.print(versionnum_x, version_y, version or PSVGAMEMANAGER_VERSION_UNK, FONT_SIZE_DEFAULT, COLOR_TEXT_POS, COLOR_BLACK, __ARIGHT)
			--列表
			local mList_y = list_y
			for i = top, bottom do
				--标记
				if psvgamemanager_list[i].mark then
					local mark_y = mList_y - mark_padding_top
					draw.fillrect(mark_x, mark_y, mark_w, mark_h, COLOR_MARK) 
				end
				
				local text_color = COLOR_TEXT_DEFAULT
				if i == psvgamemanager_pos then
					text_color = COLOR_TEXT_POS
				end
				--序数
				screen.print(ordinal_x, mList_y, string.format("%03d", i), FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				--ID
				screen.print(id_x, mList_y, psvgamemanager_list[i].id or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK)
				--游戏名
				if  i == psvgamemanager_pos and screen.textwidth(psvgamemanager_list[i].title or "") > title_w then
					title_x = screen.print(title_x, mList_y, psvgamemanager_list[i].title or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __SLEFT, title_w)
				else
					screen.print(title_x_default, mList_y, psvgamemanager_list[i].title or "", FONT_SIZE_DEFAULT, text_color, COLOR_BLACK, __ALEFT, title_w)
				end
				mList_y += FONT_LINE_HEIGHT_DEFAULT
			end
			--滚动条
			if listLen > limit then
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
			end
		elseif not PSVMANAGER_IS_LOADING then
			screen.print(list_x, list_y, PSVGAMEMANAGER_NO_GAME, FONT_SIZE_DEFAULT, COLOR_RED, COLOR_BLACK)
		end

		--顶部状态栏
		top_status_bar_draw(PSVGAME_MANAGER)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.start then --start键
			psvgamemanager_get_list()
		end

		if buttons.up or buttons.analogly < -60 then --↑键设
			if psvgamemanager_list and #psvgamemanager_list > 0 then
				if psvgamemanager_pos > 1 then
					psvgamemanager_pos -= 1
					psvgamemanager_need_refresh = true
				end
			end
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			if psvgamemanager_list and #psvgamemanager_list > 0 then
				if psvgamemanager_pos < #psvgamemanager_list then
					psvgamemanager_pos += 1
					psvgamemanager_need_refresh = true
				end
			end
		end
	
		if buttons[cancel] then --取消键
			table.remove(MATH_FUNC, #MATH_FUNC)
		end

		if buttons.square then --□键
			if psvgamemanager_list and #psvgamemanager_list > 0 then
				if psvgamemanager_list[psvgamemanager_pos].mark then
					psvgamemanager_list[psvgamemanager_pos].mark = false
				else
					psvgamemanager_list[psvgamemanager_pos].mark = true
				end
			end
		end
	
		if buttons.triangle then --△键
			psvgamemanager_open_menu()
		end
		
		if buttons[positive] then --确定键
			if psvgamemanager_list and #psvgamemanager_list > 0 and psvgamemanager_list[psvgamemanager_pos].id then
				local state = MessageDialog(TIPS, string.format(PSVGAMEMANAGER_LAUNCH_GAME_READY, psvgamemanager_list[psvgamemanager_pos].id), CANCEL, POSITIVE)
				if state == 1 then
					game.launch(psvgamemanager_list[psvgamemanager_pos].id)
				end
			end
		end
	end
		
	return math_func
	
end


