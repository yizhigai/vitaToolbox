
---------------初始化----------------------
if not SECOND_INTO_TEXTEDITOR then
	dofile("scripts/texteditor/texteditor_commons.lua")
	dofile("scripts/texteditor/texteditor_draw.lua")

	SECOND_INTO_TEXTEDITOR = true  
end

texteditor_list = {}         --文字显示列表
texteditor_list_len = 0      --文字显示列表长度
texteditor_read_pos = 0      --文件读取位置
texteditor_file_size = getFileSize(TEXTEDITOR_FILE_PATH) --文件大小

texteditor_top = 1
texteditor_pos = nil
if TEXTEDITOR_OPEN_MODE == __WRITE then
	texteditor_pos = 1	
end

__TEXTEDITOR_MODIFIED = false

if texteditorFunc == nil then
	texteditorFunc = texteditor_draw()
end
table.insert(MATH_FUNC, texteditorFunc)

texteditor_get_list()

