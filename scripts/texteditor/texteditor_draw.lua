texteditor_need_refresh = true

function texteditor_draw()

	local math_func = {}

	--按键说明
	local button_texts_ro = { --只读
		TEXTEDITOR_BUTTON_BACK,
		TEXTEDITOR_BUTTON_LR,
	}
	local button_texts_rw = { --读写
		TEXTEDITOR_BUTTON_INSERT_LINE,
		TEXTEDITOR_BUTTON_DELETE_LINE,
		TEXTEDITOR_BUTTON_POSITIVE,
		TEXTEDITOR_BUTTON_BACK,
		TEXTEDITOR_BUTTON_LR,
	}

	local limit = 18
	local bottom = limit

	--全局
	local padding_top = 18
	local padding_left = 18
	--列表
	local list_x = padding_left
	local list_y = STATUS_BAR_HEIGHT + padding_top
	local list_w = SCREEN_WIDTH - padding_left*2
	local list_h = FONT_LINE_HEIGHT_DEFAULT*limit - FONT_LINE_MARGIN_DEFAULT
	local ordinal_x = list_x --序数
	local ordinal_w =  screen.textwidth("00000")
	local text_margin_left = 8
	local text_x = ordinal_x + ordinal_w + text_margin_left --文本
	local text_w = SCREEN_WIDTH - text_x - padding_left
	local pos_padding_top = 3 --光标
	local pos_x = list_x
	local pos_w = list_w
	local pos_h = FONT_HEIGHT_DEFAULT + pos_padding_top*2
	--滚动条
	local scrollbar_track_margin_right = 6
	local scrollbar_track_w = 6
	local scrollbar_track_h = list_h
	local scrollbar_track_x = SCREEN_WIDTH - scrollbar_track_margin_right - scrollbar_track_w
	local scrollbar_track_y = list_y
	local scrollbar_w = scrollbar_track_w
	local scrollbar_h = scrollbar_track_h
	local scrollbar_x = scrollbar_track_x
	local scrollbar_y = scrollbar_track_y

	function math_func.getLimit()
		return limit
	end

	function math_func.getTextWidth()
		return text_w
	end

	-----------------构画主界面---------------------	
	function math_func.draw()
		--背景
		if TEXTEDITOR_BACKGROUND then TEXTEDITOR_BACKGROUND:blit(0, 0) end

		if texteditor_list and texteditor_list_len > 0 then
			if texteditor_need_refresh then
				texteditor_top, bottom, texteditor_pos = refreshListStates(texteditor_top, texteditor_pos, limit, texteditor_list_len)
				scrollbar_h, scrollbar_y = refreshScrollbarStates(texteditor_top, limit, texteditor_list_len, scrollbar_track_h, scrollbar_track_y)
				texteditor_need_refresh = false
			end
			--列表
			local mList_y = list_y
			for i = texteditor_top, bottom do
				--序数
				screen.print(ordinal_x, mList_y, string.format("%05d", i), FONT_SIZE_DEFAULT, COLOR_TEXTEDITOR_ORDINAL, COLOR_BLACK)
				--文本
				screen.print(text_x, mList_y, texteditor_list[i].text or "", FONT_SIZE_DEFAULT, COLOR_TEXT_DEFAULT, COLOR_BLACK)
				--光标
				if TEXTEDITOR_OPEN_MODE == __WRITE and i == texteditor_pos then
					local pos_y = mList_y - pos_padding_top
					draw.fillrect(pos_x, pos_y, pos_w, pos_h, COLOR_MARK) 
				end
				mList_y += FONT_LINE_HEIGHT_DEFAULT
			end
			--滚动条
			if texteditor_list_len > limit then
				draw.fillrect(scrollbar_track_x, scrollbar_track_y, scrollbar_track_w, scrollbar_track_h, COLOR_SCROLL_BAR_TRACK)
				draw.fillrect(scrollbar_x, scrollbar_y, scrollbar_w, scrollbar_h, COLOR_SCROLL_BAR)     
			end
		end
		
		local button_texts = button_texts_ro
		if TEXTEDITOR_OPEN_MODE == __WRITE then
			button_texts = button_texts_rw
		end
		--顶部状态栏
		top_status_bar_draw(TEXTEDITOR_FILE_NAME)
		--底部状态栏
		bottom_status_bar_draw(button_texts)
	end

	-----------------按键设置---------------------
	function math_func.ctrl()
		buttons.read()
		
		if buttons.up or buttons.analogly < -60 then --↑键设
			texteditor_scroll_up()
		elseif buttons.down or buttons.analogly > 60 then --↓鍵
			texteditor_scroll_down()
		end

		if buttons.left then --←键设
			texteditor_skip_previous_page()
		elseif buttons.right then --→键
			texteditor_skip_next_page()
		end

		if buttons.square then --□键
			texteditor_insert_line()
		end
	
		if buttons.triangle then --△键
			texteditor_remove_line()
		end
	
		if buttons[cancel] then --取消键
			texteditor_exit()
		end
		
		if buttons[positive] then --确定键
			texteditor_edit_line()
		end
	end
		
	return math_func
	
end

