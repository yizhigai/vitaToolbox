langs = {
	JAPANESE = "00",
	ENGLISH_US = "01",
	FRENCH = "02",
	SPANISH = "03",
	GERMAN = "04",
	ITALIAN = "05",
	DUTCH = "06",
	PORTUGUESE = "07",
	RUSSIAN = "08",
	KOREAN = "09",
	CHINESE_T = "10",
	CHINESE_S = "11",
	FINNISH = "12",
	SWEDISH = "13",
	DANISH = "14",
	NORWEGIAN = "15",
	POLISH = "16",
	PORTUGUESE_BR = "17",
	ENGLISH_GB = "18",
	TURKISH = "19",
}

SCREEN_WIDTH = 960
SCREEN_HEIGHT = 544
SCREEN_WIDTH_HALF = SCREEN_WIDTH/2
SCREEN_HEIGHT_HALF = SCREEN_HEIGHT/2

FONT_SIZE_DEFAULT = 1
FONT_HEIGHT_DEFAULT = 17
FONT_LINE_MARGIN_DEFAULT = 7
FONT_LINE_HEIGHT_DEFAULT = FONT_HEIGHT_DEFAULT + FONT_LINE_MARGIN_DEFAULT
FONT_SIZE_MEDIUM = 1.1
FONT_HEIGHT_MEDIUM = 19
FONT_LINE_MARGIN_MEDIUM = 7
FONT_LINE_HEIGHT_MEDIUM = FONT_HEIGHT_MEDIUM + FONT_LINE_MARGIN_MEDIUM
FONT_SIZE_LARGE = 1.2
FONT_HEIGHT_LARGE = 21
FONT_LINE_MARGIN_LARGE = 8
FONT_LINE_HEIGHT_LARGE = FONT_HEIGHT_LARGE + FONT_LINE_MARGIN_LARGE

__HOME_KEY_IS_LOCKED = false
__READ, __WRITE = 1, 2
__ZIP, __DIRECTORY = 1, 2
__COPY, __MOVE = 1, 2
__DIALOG_IS_CLOSING, __WAIT_DIALOG_IS_SHOWING, __PROGRESS_DIALOG_IS_SHOWING, __MESSAGE_DIALOG_IS_SHOWING, __MENU_DIALOG_IS_SHOWING = 0, 1, 2, 3, 4

INSTALL_APP_SCAN = false

BUTTON_CROSS = "X"
BUTTON_CIRCLE = "○"
BUTTON_TRIANGLE = "□"
BUTTON_SQUARE = "△"

TAI_CONFIG_STR0 = "# This file is used as an alternative if ux0:tai/config.txt is not found."
TAI_CONFIG_STR1 = "# For users plugins, you must refresh taiHEN from HENkaku Settings for\n# changes to take place.\n# For kernel plugins, you must reboot for changes to take place.\n*KERNEL\n# henkaku.skprx is hard-coded to load and is not listed here\n*main\n# main is a special titleid for SceShell"
TAI_CONFIG_STR2 = "ur0:tai/henkaku.suprx\n*NPXS10015\n# this is for modifying the version string\nur0:tai/henkaku.suprx\n*NPXS10016\n# this is for modifying the version string in settings widget\nur0:tai/henkaku.suprx"

PKGJ_CONFIG_GAMES_TC = "url_games http://nopaystation.com/tsv/PSV_GAMES_TC.tsv"
PKGJ_CONFIG_GAMES_SC = "url_games http://nopaystation.com/tsv/PSV_GAMES_SC.tsv"
PKGJ_CONFIG_GAMES_EN = "url_games http://nopaystation.com/tsv/PSV_GAMES.tsv"
PKGJ_CONFIG_DLCS = "url_dlcs http://nopaystation.com/tsv/PSV_DLCS.tsv"
PKGJ_CONFIG_PSX_GAMES = "url_psx_games http://nopaystation.com/tsv/PSX_GAMES.tsv"
PKGJ_CONFIG_PSP_GAMES = "url_psp_games http://nopaystation.com/tsv/PSP_GAMES.tsv"
PKGJ_CONFIG_PSM_GAMES = "url_psm_games http://nopaystation.com/tsv/PSM_GAMES.tsv"
PKGJ_CONFIG_PSP_PSX_PATH = "install_psp_psx_location %s"
PKGJ_CONFIG_PSM_DISCLAIMER = "psm_disclaimer_yes_i_read_the_readme NoPsmDrm"

