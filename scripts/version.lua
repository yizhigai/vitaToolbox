---软件版本与日期---
APP_REPO = "yizhigai"
APP_PROJECT = "vitaToolbox"

APP_VERSION_MAJOR = 0x01
APP_VERSION_MINOR = 0x64
APP_VERSION_REVISION = 0x00
APP_VERSION = ((APP_VERSION_MAJOR << 24) | (APP_VERSION_MINOR << 16) | (APP_VERSION_REVISION << 8))

APP_VERSION_STR = string.format("%x.%02x", APP_VERSION_MAJOR, APP_VERSION_MINOR)
if APP_VERSION_REVISION > 0 then
	APP_VERSION_STR = APP_VERSION_STR..string.format(".%x", APP_VERSION_REVISION)
end
APP_BUILD_DATE = "2019.10.19"
