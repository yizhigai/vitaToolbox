-----------------------theme------------------------

----------通用----------
COLOR_STROKE = 0xFD111111 --描边
COLOR_MARGIN = 0xFD111111 --间隔
COLOR_MARK   = 0x44FF7F00 --标记

----------文字----------
COLOR_TEXT_DEFAULT  = COLOR_WHITE --默认
COLOR_TEXT_POS      = COLOR_AZURE --光标
COLOR_TEXT_UNENABLE = 0xFF303030  --禁用

----------文字区域----------
COLOR_TEXT_TRACK_DEFAULT  = 0xAA333333     --默认
COLOR_TEXT_TRACK_POS      = 0xAAFF7F00    --光标

----------状态栏----------
COLOR_STATUS_BAR_TRACK = 0xAA333333 --区域

----------弹窗----------
COLOR_DIALOG_TRACK  = 0xF8333333   --区域
COLOR_DIALOG_STROKE = COLOR_STROKE --描边

----------文本编辑器----------
COLOR_TEXTEDITOR_ORDINAL = 0xFF666666 --序数

----------图片查看器----------
COLOR_IMAGEVIEWER_BG = 0xFF222222 --背景

----------滚动条----------
COLOR_SCROLL_BAR_TRACK = 0xFD444444 --区域
COLOR_SCROLL_BAR       = 0xFDFF7F00 --滚动条

----------进度条----------
COLOR_PROGRESS_BAR_TRACK = 0xFD444444 --区域
COLOR_PROGRESS_BAR       = 0xFD00FF00 --滚动条

